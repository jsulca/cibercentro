CREATE PROCEDURE usp_OrdenVenta_Emitir
	@codigo VARCHAR(20)
AS
BEGIN
	UPDATE	T1
	SET		T1.Stock -= T2.Cantidad
	FROM gen.Producto T1
	INNER JOIN ven.OrdenVentaDetalle T2 ON T1.ProductoId = T2.ProductoId
	WHERE T2.CodigoOrdenVenta = @codigo
END
GO

CREATE PROCEDURE usp_OrdenVenta_Anular
	@codigo VARCHAR(20)
AS
BEGIN
	UPDATE	T1
	SET		T1.Stock += T2.Cantidad
	FROM gen.Producto T1
	INNER JOIN ven.OrdenVentaDetalle T2 ON T1.ProductoId = T2.ProductoId
	WHERE T2.CodigoOrdenVenta = @codigo
END
GO

CREATE PROCEDURE usp_Pago_Descontar
	@id INT
AS
BEGIN
	UPDATE	T1
	SET		T1.Estado = 2
	FROM	mov.Descuento T1
	INNER JOIN mov.PagoDescuento T2 ON T1.DescuentoId = T2.DescuentoId 
	WHERE	T2.PagoId = @id
	AND		T1.Estado = 1
END