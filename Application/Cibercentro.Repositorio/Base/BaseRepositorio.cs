﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Data.Entity;
using Cibercentro.Contexto;

namespace Cibercentro.Repositorio
{
    public class BaseRepositorio<T> where T : class
    {
        public T GetOne(Expression<Func<T, bool>> predicate)
        {
            using (var ctx = new CibercentroContext())
            {
                return ctx.Set<T>().SingleOrDefault(predicate);
            }
        }

        public List<T> GetAll()
        {
            using (var ctx = new CibercentroContext())
            {
                return ctx.Set<T>().ToList();
            }
        }

        public List<T> GetAllBy(Expression<Func<T, bool>> predicate)
        {
            using (var ctx = new CibercentroContext())
            {
                return ctx.Set<T>().Where(predicate).ToList<T>();
            }
        }

        public int CountBy(Expression<Func<T, bool>> predicate)
        {
            using (var ctx = new CibercentroContext())
            {
                return ctx.Set<T>().Count(predicate);
            }
        }

        public int Count()
        {
            using (var ctx = new CibercentroContext())
            {
                return ctx.Set<T>().Count();
            }
        }

    }
}
