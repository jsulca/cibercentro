﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Cibercentro.Contexto;
using Cibercentro.Entidades;

namespace Cibercentro.Repositorio
{
    public class PagoRepositorio : BaseRepositorio<Pago>
    {
        public Pago GetOne(int id)
        {
            try
            {
                using (var ctx = new CibercentroContext())
                {
                    return ctx.Pago.Where(x => x.PagoId == id)
                        .Include(x => x.Descuentos)
                        .Include(x => x.Descuentos.Select(y => y.Descuento))
                        .SingleOrDefault();
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public void Save(Pago entity)
        {
            try
            {
                using (var ctx = new CibercentroContext())
                {
                    ctx.Entry(entity).State = EntityState.Added;
                    ctx.SaveChanges();
                    ctx.Database.ExecuteSqlCommand("EXEC usp_Pago_Descontar @p0", entity.PagoId);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
