﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Cibercentro.Entidades;
using Cibercentro.Contexto;

namespace Cibercentro.Repositorio
{
    public class DescuentoRepositorio : BaseRepositorio<Descuento>
    {
        public void Save(Descuento entity)
        {
            try
            {
                using (var ctx = new CibercentroContext())
                {
                    ctx.Entry(entity).State = entity.DescuentoId > 0 ? EntityState.Modified : EntityState.Added;
                    ctx.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Descuento> GetAllLikePagin(Descuento filter, int pageIndex, int pageSize, ref int totalRows)
        {
            try
            {
                using (var ctx = new CibercentroContext())
                {
                    var source = (from d in ctx.Descuento
                                  join e in ctx.Empleado on d.EmpleadoId equals e.EmpleadoId
                                  orderby d.FechaCreacion descending
                                  select new
                                  {
                                      descuento = d,
                                      empleadonombre = e.Nombre,
                                      empleadoapellido = e.Apellido
                                  });
                    if (filter != null)
                    {

                    }
                    totalRows = source.Count();
                    var data = source.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
                    data.ForEach(x =>
                    {
                        x.descuento.EmpleadoNombreCompleto = $"{x.empleadoapellido}, {x.empleadonombre}";
                    });
                    return data.Select(x => x.descuento).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Descuento GetOne(int id)
        {
            try
            {
                using(var ctx = new CibercentroContext())
                {

                    var source = (from d in ctx.Descuento
                                  join e in ctx.Empleado on d.EmpleadoId equals e.EmpleadoId
                                  where d.DescuentoId == id
                                  select new
                                  {
                                      descuento = d,
                                      empleadonombre = e.Nombre,
                                      empleadoapellido = e.Apellido
                                  }).SingleOrDefault();
                    source.descuento.EmpleadoNombreCompleto = $"{source.empleadoapellido}, {source.empleadonombre}";
                    return source.descuento;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
