﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Cibercentro.Contexto;
using Cibercentro.Entidades;
using Cibercentro.Entidades.Filtro;

namespace Cibercentro.Repositorio
{
    public class IngresoRepositorio : BaseRepositorio<Ingreso>
    {
        public List<Ingreso> GetAllLikePagin(IngresoFiltro filter, int pageSize, int pageIndex, ref int totalRows)
        {
            try
            {
                using (var ctx = new CibercentroContext())
                {
                    var source = (from i in ctx.Ingreso
                                  join e in ctx.Empleado on i.EmpleadoId equals e.EmpleadoId
                                  orderby i.FechaRegistro descending
                                  select new
                                  {
                                      ingreso = i,
                                      empleadonombre = e.Nombre,
                                      empleadoapellido = e.Apellido
                                  });
                    if (filter != null)
                    {
                        if (!string.IsNullOrEmpty(filter.EmpleadoNombre) && !string.IsNullOrWhiteSpace(filter.EmpleadoNombre)) source = source.Where(x => (x.empleadoapellido + " " + x.empleadonombre).Contains(filter.EmpleadoNombre));
                        if (filter.FechaDesde.HasValue)
                        {
                            if (filter.FechaHasta.HasValue) source = source.Where(x => x.ingreso.FechaRegistro >= filter.FechaDesde.Value && x.ingreso.FechaRegistro <= filter.FechaHasta.Value);
                            else source = source.Where(x => x.ingreso.FechaRegistro >= filter.FechaDesde.Value);
                        }

                        if (filter.CantidadDesde.HasValue) source = source.Where(x => x.ingreso.Cantidad >= filter.CantidadDesde.Value);
                        if (filter.CantidadHasta.HasValue) source = source.Where(x => x.ingreso.Cantidad <= filter.CantidadHasta.Value);
                        //{
                        //    if (filter.CantidadHasta.HasValue) source = source.Where(x => x.ingreso.Cantidad >= filter.CantidadDesde.Value && x.ingreso.Cantidad <= filter.CantidadHasta.Value);
                        //    else source = source.Where(x => x.ingreso.Cantidad >= filter.CantidadDesde.Value);
                        //}
                    }
                    totalRows = source.Count();
                    var data = source.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
                    data.ForEach(x => x.ingreso.EmpleadoNombreCompleto = $"{x.empleadoapellido}, {x.empleadonombre}");
                    return data.Select(x => x.ingreso).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(Ingreso entity)
        {
            try
            {
                using (var ctx = new CibercentroContext())
                {
                    if (entity.IngresoId > 0 && entity.Detalles != null)
                    {
                        entity.Detalles.ForEach(x => ctx.Entry(x).State = EntityState.Modified);
                    }
                    ctx.Entry(entity).State = entity.IngresoId > 0 ? EntityState.Modified : EntityState.Added;
                    ctx.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Ingreso GetOne(int id)
        {
            try
            {
                using (var ctx = new CibercentroContext())
                {
                    var source = (from i in ctx.Ingreso
                                  join e in ctx.Empleado on i.EmpleadoId equals e.EmpleadoId
                                  join det in ctx.IngresoDetalle on i.IngresoId equals det.IngresoId into Detalle
                                  where i.IngresoId == id
                                  select new
                                  {
                                      ingreso = i,
                                      empleadonombre = e.Nombre,
                                      empleadoapellido = e.Apellido,
                                      detalle = Detalle,
                                      tipoIngreso = Detalle.Select(x => x.TipoIngreso)
                                  }).SingleOrDefault();
                    source.ingreso.EmpleadoNombreCompleto = $"{source.empleadoapellido}, {source.empleadonombre}";
                    return source.ingreso;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Ingreso> GetAllByDates(DateTime desde, DateTime hasta)
        {
            try
            {
                using (var ctx = new CibercentroContext())
                {
                    return ctx.Ingreso.Where(x => x.FechaRegistro >= desde && x.FechaRegistro <= hasta).Include(x => x.Detalles).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
