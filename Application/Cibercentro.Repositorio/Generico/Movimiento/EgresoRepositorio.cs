﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Cibercentro.Contexto;
using Cibercentro.Entidades;

namespace Cibercentro.Repositorio
{
    public class EgresoRepositorio : BaseRepositorio<Egreso>
    {
        public List<Egreso> GetAllLikePagin(Egreso filter, int pageSize, int pageIndex, ref int totalRows)
        {
            try
            {
                using (var ctx = new CibercentroContext())
                {
                    var source = (from e in ctx.Egreso
                                  join em in ctx.Empleado on e.EmpleadoId equals em.EmpleadoId
                                  orderby e.FechaRegistro descending
                                  select new
                                  {
                                      egreso = e,
                                      empleadonombre = em.Nombre,
                                      empleadoapellido = em.Apellido
                                  });
                    if (filter != null)
                    {
                    }
                    totalRows = source.Count();
                    var data = source.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
                    data.ForEach(x => x.egreso.EmpleadoNombreCompleto = $"{x.empleadoapellido}, {x.empleadonombre}");
                    return data.Select(x => x.egreso).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(Egreso entity)
        {
            try
            {
                using (var ctx = new CibercentroContext())
                {
                    ctx.Entry(entity).State = entity.EgresoId > 0 ? EntityState.Modified : EntityState.Added;
                    ctx.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Egreso GetOne(int id)
        {
            try
            {
                using (var ctx = new CibercentroContext())
                {
                    var source = (from e in ctx.Egreso
                                  join em in ctx.Empleado on e.EmpleadoId equals em.EmpleadoId
                                  where e.EgresoId == id
                                  select new
                                  {
                                      egreso = e,
                                      empleadonombre = em.Nombre,
                                      empleadoapellido = em.Apellido
                                  }).SingleOrDefault();
                    source.egreso.EmpleadoNombreCompleto = $"{source.empleadoapellido}, {source.empleadonombre}";
                    return source.egreso;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
