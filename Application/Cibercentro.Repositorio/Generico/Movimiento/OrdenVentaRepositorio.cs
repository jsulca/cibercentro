﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Cibercentro.Contexto;
using Cibercentro.Entidades;
using Cibercentro.Entidades.Filtro;

namespace Cibercentro.Repositorio
{
    public class OrdenVentaRepositorio : BaseRepositorio<OrdenVenta>
    {
        public List<OrdenVenta> GetAllLikePagin(OrdenVentaFiltro filter, int pageSize, int pageIndex, ref int totalRows)
        {
            try
            {
                using (var ctx = new CibercentroContext())
                {
                    var source = (from ov in ctx.OrdenVenta
                                  join e in ctx.Empleado on ov.EmpleadoCreacionId equals e.EmpleadoId
                                  orderby ov.FechaCreacion descending
                                  select new
                                  {
                                      ordenventa = ov,
                                      empleadonombre = e.Nombre,
                                      empleadoapellido = e.Apellido
                                  });
                    if (filter != null)
                    {
                        if (!string.IsNullOrEmpty(filter.Codigo) && !string.IsNullOrWhiteSpace(filter.Codigo)) source = source.Where(x => x.ordenventa.Codigo.Contains(filter.Codigo));
                        if (!string.IsNullOrEmpty(filter.Empleado) && !string.IsNullOrWhiteSpace(filter.Empleado)) source = source.Where(x => x.empleadonombre.Contains(filter.Empleado));
                    }
                    totalRows = source.Count();
                    var data = source.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
                    data.ForEach(x => x.ordenventa.EmpleadoCreacionNombreCompleto = $"{x.empleadoapellido}, {x.empleadonombre}");
                    return data.Select(x => x.ordenventa).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(OrdenVenta entity)
        {
            try
            {
                using (var ctx = new CibercentroContext())
                {
                    bool agregar = ctx.OrdenVenta.Count(x => x.Codigo.Equals(entity.Codigo)) == 0;
                    if (agregar)
                    {
                        Configuracion configuracion = ctx.Configuracion.Single();
                        entity.Codigo = configuracion.CorrelativoOrdenVenta.ToString("V-000000000000000");
                        configuracion.CorrelativoOrdenVenta++;
                        ctx.Entry(configuracion).State = EntityState.Modified;
                    }
                    else ctx.OrdenVentaDetalle.RemoveRange(ctx.OrdenVentaDetalle.Where(x => x.CodigoOrdenVenta.Equals(entity.Codigo)).ToList());
                    
                    entity.Detalles.ForEach(x => {
                        x.CodigoOrdenVenta = entity.Codigo;
                        ctx.Entry(x).State = EntityState.Added;
                    });
                    ctx.Entry(entity).State = agregar ? EntityState.Added : EntityState.Modified;
                    ctx.SaveChanges();
                    ctx.Database.ExecuteSqlCommand("EXEC usp_OrdenVenta_Emitir @p0", entity.Codigo);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Anular(OrdenVenta entity)
        {
            try
            {
                using (var ctx = new CibercentroContext())
                {
                    ctx.Entry(entity).State = EntityState.Modified;
                    ctx.SaveChanges();
                    ctx.Database.ExecuteSqlCommand("EXEC usp_OrdenVenta_Anular @p0", entity.Codigo);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public OrdenVenta GetOne(string id)
        {
            try
            {
                using (var ctx = new CibercentroContext())
                {
                    return ctx.OrdenVenta
                            .Include(x => x.Detalles)
                            .Include(x => x.Detalles.Select(y => y.Producto))
                            .SingleOrDefault(x => x.Codigo.Equals(id));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
