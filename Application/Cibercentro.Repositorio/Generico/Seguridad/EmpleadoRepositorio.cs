﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Cibercentro.Contexto;
using Cibercentro.Entidades;
using Cibercentro.Entidades.Filtro;

namespace Cibercentro.Repositorio
{
    public partial class EmpleadoRepositorio : BaseRepositorio<Empleado>
    {
        public List<Empleado> GetAllLikePagin(EmpleadoFiltro filter, int pageSize, int pageIndex, ref int totalRows)
        {
            try
            {
                using (var ctx = new CibercentroContext())
                {
                    var source = (from u in ctx.Empleado
                                  join r in ctx.Rol on u.RolId equals r.RolId
                                  orderby u.EmpleadoId
                                  select new
                                  {
                                      empleado = u,
                                      rol = r
                                  });
                    if (filter != null)
                    {
                        if (!string.IsNullOrEmpty(filter.Rol) && !string.IsNullOrWhiteSpace(filter.Rol)) source = source.Where(x => x.rol.Nombre.Contains(filter.Rol));
                        if (!string.IsNullOrEmpty(filter.Nombre) && !string.IsNullOrWhiteSpace(filter.Nombre)) source = source.Where(x => x.empleado.Nombre.Contains(filter.Nombre));
                        if (!string.IsNullOrEmpty(filter.Apellido) && !string.IsNullOrWhiteSpace(filter.Apellido)) source = source.Where(x => x.empleado.Apellido.Contains(filter.Apellido));
                        if (!string.IsNullOrEmpty(filter.NroDocumento) && !string.IsNullOrWhiteSpace(filter.NroDocumento)) source = source.Where(x => x.empleado.NroDocumento.Contains(filter.NroDocumento));
                        if (!string.IsNullOrEmpty(filter.Correo) && !string.IsNullOrWhiteSpace(filter.Correo)) source = source.Where(x => x.empleado.Correo.Contains(filter.Correo));
                    }
                    totalRows = source.Count();
                    var data = source.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
                    data.ForEach(x =>
                    {
                        x.empleado.Rol = x.rol;
                    });
                    return data.Select(x => x.empleado).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Empleado> GetFirst(Empleado filter, int pageSize)
        {
            try
            {
                using (var ctx = new CibercentroContext())
                {
                    var source = (from e in ctx.Empleado
                                  where e.Nombre.Contains(filter.Nombre ?? e.Nombre) || e.Apellido.Contains(filter.Apellido ?? e.Apellido)
                                  select e);
                    var data = source.Take(pageSize).ToList();
                    return data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(Empleado entity)
        {
            try
            {
                using (var ctx = new CibercentroContext())
                {
                    ctx.Entry(entity).State = entity.EmpleadoId > 0 ? EntityState.Modified : EntityState.Added;
                    ctx.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Empleado GetOne(int id)
        {
            try
            {
                using (var ctx = new CibercentroContext())
                {
                    var source = (from u in ctx.Empleado
                                  join r in ctx.Rol on u.RolId equals r.RolId
                                  where u.EmpleadoId == id
                                  select new
                                  {
                                      usuario = u,
                                      rol = r
                                  }).SingleOrDefault();
                    source.usuario.Rol = source.rol;

                    return source.usuario;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Empleado GetOneByLogin(string nroDocumento, int tipoDocumento)
        {
            try
            {
                using (var ctx = new CibercentroContext())
                {
                    return ctx.Empleado
                            .SingleOrDefault(x => (int)x.TipoDocumento == tipoDocumento && x.NroDocumento.Equals(nroDocumento));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

    }
}
