﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Cibercentro.Contexto;
using Cibercentro.Entidades;

namespace Cibercentro.Repositorio
{
    public partial class RolRepositorio : BaseRepositorio<Rol>
    {
        public List<Rol> GetAllLikePagin(Rol filter, int pageSize, int pageIndex, ref int totalRows)
        {
            try
            {
                using (var ctx = new CibercentroContext())
                {
                    var source = ctx.Rol.AsQueryable();
                    if (filter != null)
                    {
                        if (!string.IsNullOrEmpty(filter.Nombre)) source = source.Where(x => x.Nombre.Contains(filter.Nombre));
                    }
                    totalRows = source.Count();
                    var data = source.OrderBy(x => x.RolId).Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
                    return data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Save(Rol entity)
        {
            try
            {
                using (var ctx = new CibercentroContext())
                {
                    if(entity.Menus != null)
                    {
                        var menus = ctx.RolMenu.Where(x => x.RolId == entity.RolId).ToList();
                        ctx.RolMenu.RemoveRange(menus);
                        entity.Menus.ForEach(x => {
                            ctx.Entry(x).State = EntityState.Added;
                        });
                        var paginas = ctx.RolPagina.Where(x => x.RolId == entity.RolId).ToList();
                        ctx.RolPagina.RemoveRange(paginas);
                        entity.Paginas.ForEach(x => {
                            ctx.Entry(x).State = EntityState.Added;
                        });
                        var controles = ctx.RolControl.Where(x => x.RolId == entity.RolId).ToList();
                        ctx.RolControl.RemoveRange(controles);
                        entity.Controles.ForEach(x => {
                            ctx.Entry(x).State = EntityState.Added;
                        });
                    }
                    ctx.Entry(entity).State = entity.RolId > 0 ? EntityState.Modified : EntityState.Added;
                    ctx.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public Rol GetOne(int id)
        {
            try
            {
                using (var ctx = new CibercentroContext())
                {
                    return ctx.Rol.Include(x => x.Paginas)
                                .Include(x => x.Controles)
                                .Include(x => x.Menus)
                                .SingleOrDefault(x => x.RolId == id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public Rol GetOneByLogin(int id)
        {
            try
            {
                using (var ctx = new CibercentroContext())
                {
                    return ctx.Rol.Include(x => x.Menus)
                                .Include(x => x.Menus.Select(y => y.Menu))
                                .Include(x => x.Paginas)
                                .Include(x => x.Paginas.Select(y => y.Pagina))
                                .Include(x => x.Controles)
                                .Include(x => x.Controles.Select(y => y.Control))
                                .SingleOrDefault(x => x.RolId == id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
