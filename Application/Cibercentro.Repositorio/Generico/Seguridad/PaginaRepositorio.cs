﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Cibercentro.Contexto;
using Cibercentro.Entidades;

namespace Cibercentro.Repositorio
{
    public partial class PaginaRepositorio : BaseRepositorio<Pagina>
    {
        public List<Pagina> GetAllLikePagin(Pagina filter, int pageSize, int pageIndex, ref int totalRows)
        {
            try
            {
                using (var ctx = new CibercentroContext())
                {
                    var source = ctx.Pagina.AsQueryable();
                    if (filter != null)
                    {
                        if (!string.IsNullOrEmpty(filter.Nombre)) source = source.Where(x => x.Nombre.Contains(filter.Nombre));
                        if (!string.IsNullOrEmpty(filter.Controlador)) source = source.Where(x => x.Controlador.Contains(filter.Controlador));
                        if (!string.IsNullOrEmpty(filter.Accion)) source = source.Where(x => x.Accion.Contains(filter.Accion));
                    }
                    totalRows = source.Count();
                    var data = source.OrderBy(x => x.PaginaId).Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
                    return data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Save(Pagina entity)
        {
            try
            {
                using (var ctx = new CibercentroContext())
                {
                    if (entity.Controles != null)
                    {
                        entity.Controles.ForEach(x =>
                        {
                            if (x.Eliminado) ctx.Entry(x).State = EntityState.Deleted;
                            else ctx.Entry(x).State = x.ControlId > 0 ? EntityState.Modified : EntityState.Added;
                        });
                        entity.Controles.RemoveAll(x => x.Eliminado);
                    }
                    ctx.Entry(entity).State = entity.PaginaId > 0 ? EntityState.Modified : EntityState.Added;
                    ctx.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public Pagina GetOne(int id)
        {
            try
            {
                using (var ctx = new CibercentroContext())
                {
                    return ctx.Pagina
                            .Include(x => x.Controles)
                            .SingleOrDefault(x => x.PaginaId == id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<Pagina> GetAllWithControls()
        {
            try
            {
                using (var ctx = new CibercentroContext())
                {
                    return ctx.Pagina.Where(x => x.Estado == EstadoPagina.Activo)
                                .Include(x => x.Controles)
                                .ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
