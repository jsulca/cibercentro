﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Cibercentro.Contexto;
using Cibercentro.Entidades;

namespace Cibercentro.Repositorio
{
    public class TipoIngresoRepositorio : BaseRepositorio<TipoIngreso>
    {
        public List<TipoIngreso> GetAllLikePagin(TipoIngreso filter, int pageSize, int pageIndex, ref int totalRows)
        {
            try
            {
                using (var ctx = new CibercentroContext())
                {
                    var source = ctx.TipoIngreso.OrderBy(x => x.TipoIngresoId).AsQueryable();
                    if (filter != null)
                    {
                    }
                    totalRows = source.Count();
                    var data = source.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
                    //data.ForEach(x =>
                    //{
                    //});
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(TipoIngreso entity)
        {
            try
            {
                using (var ctx = new CibercentroContext())
                {
                    ctx.Entry(entity).State = entity.TipoIngresoId > 0 ? EntityState.Modified : EntityState.Added;
                    ctx.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public TipoIngreso GetOne(int id)
        {
            try
            {
                using (var ctx = new CibercentroContext())
                {
                    return ctx.TipoIngreso.SingleOrDefault(x => x.TipoIngresoId == id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
