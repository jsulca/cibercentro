﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Cibercentro.Contexto;
using Cibercentro.Entidades;
using Cibercentro.Entidades.Filtro;

namespace Cibercentro.Repositorio
{
    public class ProductoRepositorio : BaseRepositorio<Producto>
    {
        public List<Producto> GetAllLikePagin(ProductoFiltro filter, int pageSize, int pageIndex, ref int totalRows)
        {
            try
            {
                using (var ctx = new CibercentroContext())
                {
                    var source = ctx.Producto.OrderBy(x => x.Codigo).AsQueryable();
                    if (filter != null)
                    {
                        if (!string.IsNullOrEmpty(filter.Codigo) && !string.IsNullOrWhiteSpace(filter.Codigo)) source = source.Where(x => x.Codigo.Contains(filter.Codigo));
                        if (!string.IsNullOrEmpty(filter.Nombre) && !string.IsNullOrWhiteSpace(filter.Nombre)) source = source.Where(x => x.Nombre.Contains(filter.Nombre));
                    }
                    totalRows = source.Count();
                    var data = source.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(Producto entity)
        {
            try
            {
                using (var ctx = new CibercentroContext())
                {
                    ctx.Entry(entity).State = entity.ProductoId == 0 ? EntityState.Added : EntityState.Modified;
                    ctx.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
