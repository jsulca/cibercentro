﻿namespace Cibercentro.Contexto
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using Entidades;
    using Configuracion;

    public class CibercentroContext : DbContext
    {
        public DbSet<Empleado> Empleado { get; set; }
        public DbSet<Rol> Rol { get; set; }
        public DbSet<Pagina> Pagina { get; set; }
        public DbSet<Control> Control { get; set; }
        public DbSet<Menu> Menu { get; set; }
        public DbSet<RolPagina> RolPagina { get; set; }
        public DbSet<RolControl> RolControl { get; set; }
        public DbSet<RolMenu> RolMenu { get; set; }

        public DbSet<Ingreso> Ingreso { get; set; }
        public DbSet<Egreso> Egreso { get; set; }
        public DbSet<IngresoDetalle> IngresoDetalle { get; set; }

        public DbSet<TipoIngreso> TipoIngreso { get; set; }

        public DbSet<Descuento> Descuento { get; set; }

        public DbSet<Producto> Producto { get; set; }

        public DbSet<OrdenVenta> OrdenVenta { get; set; }
        public DbSet<OrdenVentaDetalle> OrdenVentaDetalle { get; set; }
        public DbSet<Entidades.Configuracion> Configuracion { get; set; }

        public DbSet<Pago> Pago { get; set; }
        public DbSet<PagoDescuento> PagoDescuento { get; set; }

        public CibercentroContext()
            : base("name=CibercentroContext")
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            #region Seguridad

            new EmpleadoConfig(modelBuilder.Entity<Empleado>());
            new RolConfig(modelBuilder.Entity<Rol>());
            new PaginaConfig(modelBuilder.Entity<Pagina>());
            new ControlConfig(modelBuilder.Entity<Control>());
            new MenuConfig(modelBuilder.Entity<Menu>());

            new RolPaginaConfig(modelBuilder.Entity<RolPagina>());
            new RolControlConfig(modelBuilder.Entity<RolControl>());
            new RolMenuConfig(modelBuilder.Entity<RolMenu>());

            #endregion

            #region Movimiento

            new IngresoConfig(modelBuilder.Entity<Ingreso>());
            new IngresoDetalleConfig(modelBuilder.Entity<IngresoDetalle>());
            new EgresoConfig(modelBuilder.Entity<Egreso>());
            new DescuentoConfig(modelBuilder.Entity<Descuento>());

            new PagoConfig(modelBuilder.Entity<Pago>());
            new PagoDescuentoConfig(modelBuilder.Entity<PagoDescuento>());

            #endregion

            #region General

            new TipoIngresoConfig(modelBuilder.Entity<TipoIngreso>());
            new ProductoConfig(modelBuilder.Entity<Producto>());

            #endregion

            #region General

            new OrdenVentaConfig(modelBuilder.Entity<OrdenVenta>());
            new OrdenVentaDetalleConfig(modelBuilder.Entity<OrdenVentaDetalle>());
            new ConfiguracionConfig(modelBuilder.Entity<Entidades.Configuracion>());

            #endregion

            base.OnModelCreating(modelBuilder);
        }
    }
}