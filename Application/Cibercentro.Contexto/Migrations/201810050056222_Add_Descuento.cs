namespace Cibercentro.Contexto.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_Descuento : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "mov.Descuento",
                c => new
                    {
                        DescuentoId = c.Int(nullable: false, identity: true),
                        EmpleadoId = c.Int(nullable: false),
                        Cantidad = c.Decimal(nullable: false, precision: 22, scale: 2),
                        Motivo = c.String(nullable: false, maxLength: 2000),
                        EmpleadoCreacionId = c.Int(nullable: false),
                        EmpleadoActualizacionId = c.Int(),
                        FechaCreacion = c.DateTime(nullable: false),
                        FechaActualizacion = c.DateTime(),
                    })
                .PrimaryKey(t => t.DescuentoId);
            
        }
        
        public override void Down()
        {
            DropTable("mov.Descuento");
        }
    }
}
