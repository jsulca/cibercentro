namespace Cibercentro.Contexto.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update_Producto_3 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("gen.Producto", "Codigo", c => c.String(maxLength: 50));
        }
        
        public override void Down()
        {
            AlterColumn("gen.Producto", "Codigo", c => c.String(nullable: false, maxLength: 50));
        }
    }
}
