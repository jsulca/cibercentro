namespace Cibercentro.Contexto.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<CibercentroContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(CibercentroContext context)
        {
            // context.Menu.AddOrUpdate(
            //    new Entidades.Menu() { MenuId = 1, PadreId = 0, Nombre = "Seguridad", Icono = null, Tipo = Entidades.TipoMenu.Collapse, Url = null },
            //    new Entidades.Menu() { MenuId = 2, PadreId = 1, Nombre = "Empleado", Icono = null, Tipo = Entidades.TipoMenu.Item, Url = "/Empleado/Index" },
            //    new Entidades.Menu() { MenuId = 3, PadreId = 1, Nombre = "Rol", Icono = null, Tipo = Entidades.TipoMenu.Item, Url = "/Rol/Index" },
            //    new Entidades.Menu() { MenuId = 4, PadreId = 0, Nombre = "Movimientos", Icono = null, Tipo = Entidades.TipoMenu.Collapse, Url = null },
            //    new Entidades.Menu() { MenuId = 5, PadreId = 4, Nombre = "Ingresos", Icono = null, Tipo = Entidades.TipoMenu.Item, Url = "/Ingreso/Index" },
            //    new Entidades.Menu() { MenuId = 6, PadreId = 4, Nombre = "Egresos", Icono = null, Tipo = Entidades.TipoMenu.Item, Url = "/Egreso/Index" },
            //    new Entidades.Menu() { MenuId = 7, PadreId = 0, Nombre = "General", Icono = null, Tipo = Entidades.TipoMenu.Collapse, Url = null },
            //    new Entidades.Menu() { MenuId = 8, PadreId = 7, Nombre = "Tipo de Ingresos", Icono = null, Tipo = Entidades.TipoMenu.Item, Url = "/TipoIngreso/Index" },
            //    new Entidades.Menu() { MenuId = 9, PadreId = 0, Nombre = "Reportes", Icono = null, Tipo = Entidades.TipoMenu.Collapse, Url = null },
            //    new Entidades.Menu() { MenuId = 10, PadreId = 9, Nombre = "Ingresos", Icono = null, Tipo = Entidades.TipoMenu.Item, Url = "/Reporte/Ingreso" },
            //    new Entidades.Menu() { MenuId = 11, PadreId = 9, Nombre = "Egresos", Icono = null, Tipo = Entidades.TipoMenu.Item, Url = "/Reporte/Egreso" },
            //    new Entidades.Menu() { MenuId = 12, PadreId = 9, Nombre = "Consolidado", Icono = null, Tipo = Entidades.TipoMenu.Item, Url = "/Reporte/Consolidado" },
            //    new Entidades.Menu() { MenuId = 13, PadreId = 4, Nombre = "Descuento", Icono = null, Tipo = Entidades.TipoMenu.Item, Url = "/Descuento/Index" },
            //    new Entidades.Menu() { MenuId = 14, PadreId = 7, Nombre = "Producto", Icono = null, Tipo = Entidades.TipoMenu.Item, Url = "/Producto/Index" },
            //    new Entidades.Menu() { MenuId = 15, PadreId = 4, Nombre = "Venta", Icono = null, Tipo = Entidades.TipoMenu.Item, Url = "/OrdenVenta/Index" }
            //);
            context.Pagina.AddOrUpdate(
                new Entidades.Pagina() { PaginaId = 1, Controlador = "Rol", Accion = "Index", Nombre = "Lista de roles", Estado = Entidades.EstadoPagina.Activo },
                new Entidades.Pagina() { PaginaId = 2, Controlador = "Rol", Accion = "Editar", Nombre = "Editar Rol", Estado = Entidades.EstadoPagina.Activo },
                new Entidades.Pagina() { PaginaId = 3, Controlador = "Empleado", Accion = "Index", Nombre = "Lista de empleados", Estado = Entidades.EstadoPagina.Activo },
                new Entidades.Pagina() { PaginaId = 4, Controlador = "Empleado", Accion = "Editar", Nombre = "Editar Empleado", Estado = Entidades.EstadoPagina.Activo },

                new Entidades.Pagina() { PaginaId = 5, Controlador = "Ingreso", Accion = "Index", Nombre = "Lista de ingresos", Estado = Entidades.EstadoPagina.Activo },
                new Entidades.Pagina() { PaginaId = 6, Controlador = "Ingreso", Accion = "Editar", Nombre = "Editar Ingreso", Estado = Entidades.EstadoPagina.Activo },

                new Entidades.Pagina() { PaginaId = 7, Controlador = "Egreso", Accion = "Index", Nombre = "Lista de egresos", Estado = Entidades.EstadoPagina.Activo },
                new Entidades.Pagina() { PaginaId = 8, Controlador = "Egreso", Accion = "Editar", Nombre = "Editar Egreso", Estado = Entidades.EstadoPagina.Activo },

                new Entidades.Pagina() { PaginaId = 9, Controlador = "TipoIngreso", Accion = "Index", Nombre = "Tipos de ingreso", Estado = Entidades.EstadoPagina.Activo },
                new Entidades.Pagina() { PaginaId = 10, Controlador = "TipoIngreso", Accion = "Editar", Nombre = "Editar Tipo de Ingreso", Estado = Entidades.EstadoPagina.Activo },

                new Entidades.Pagina() { PaginaId = 11, Controlador = "Reporte", Accion = "Ingreso", Nombre = "Reporte de ingresos", Estado = Entidades.EstadoPagina.Activo },
                new Entidades.Pagina() { PaginaId = 12, Controlador = "Reporte", Accion = "Egreso", Nombre = "Reporte de egresos", Estado = Entidades.EstadoPagina.Activo },
                new Entidades.Pagina() { PaginaId = 13, Controlador = "Reporte", Accion = "Consolidado", Nombre = "Reporte consolidado", Estado = Entidades.EstadoPagina.Activo },

                new Entidades.Pagina() { PaginaId = 14, Controlador = "Descuento", Accion = "Index", Nombre = "Lista de descuentos", Estado = Entidades.EstadoPagina.Activo },
                new Entidades.Pagina() { PaginaId = 15, Controlador = "Descuento", Accion = "Editar", Nombre = "Editar Descuento", Estado = Entidades.EstadoPagina.Activo },

                new Entidades.Pagina() { PaginaId = 16, Controlador = "Producto", Accion = "Index", Nombre = "Lista de productos", Estado = Entidades.EstadoPagina.Activo },
                new Entidades.Pagina() { PaginaId = 17, Controlador = "Producto", Accion = "Editar", Nombre = "Editar producto", Estado = Entidades.EstadoPagina.Activo },

                new Entidades.Pagina() { PaginaId = 18, Controlador = "OrdenVenta", Accion = "Index", Nombre = "Lista de Ordenes de Venta", Estado = Entidades.EstadoPagina.Activo },
                new Entidades.Pagina() { PaginaId = 19, Controlador = "OrdenVenta", Accion = "Editar", Nombre = "Editar Orden de Venta", Estado = Entidades.EstadoPagina.Activo },

                new Entidades.Pagina() { PaginaId = 20, Controlador = "Pago", Accion = "Index", Nombre = "Lista de Ordenes Pagos", Estado = Entidades.EstadoPagina.Activo },
                new Entidades.Pagina() { PaginaId = 21, Controlador = "Pago", Accion = "Nuevo", Nombre = "Generar Pago", Estado = Entidades.EstadoPagina.Activo }
            );
            // context.Control.AddOrUpdate(
            //     new Entidades.Control() { ControlId = 1, PaginaId = 1, Nombre = "Nuevo" },
            //     new Entidades.Control() { ControlId = 2, PaginaId = 1, Nombre = "Ver" },
            //     new Entidades.Control() { ControlId = 3, PaginaId = 2, Nombre = "Grabar" },
            //     new Entidades.Control() { ControlId = 4, PaginaId = 3, Nombre = "Nuevo" },
            //     new Entidades.Control() { ControlId = 5, PaginaId = 3, Nombre = "Ver" },
            //     new Entidades.Control() { ControlId = 6, PaginaId = 4, Nombre = "Grabar" },

            //     new Entidades.Control() { ControlId = 7, PaginaId = 5, Nombre = "Nuevo" },
            //     new Entidades.Control() { ControlId = 8, PaginaId = 5, Nombre = "Ver" },
            //     new Entidades.Control() { ControlId = 9, PaginaId = 6, Nombre = "Grabar" },


            //     new Entidades.Control() { ControlId = 10, PaginaId = 7, Nombre = "Nuevo" },
            //     new Entidades.Control() { ControlId = 11, PaginaId = 7, Nombre = "Ver" },
            //     new Entidades.Control() { ControlId = 12, PaginaId = 8, Nombre = "Grabar" },


            //     new Entidades.Control() { ControlId = 13, PaginaId = 9, Nombre = "Nuevo" },
            //     new Entidades.Control() { ControlId = 14, PaginaId = 9, Nombre = "Ver" },
            //     new Entidades.Control() { ControlId = 15, PaginaId = 10, Nombre = "Grabar" },

            //     new Entidades.Control() { ControlId = 16, PaginaId = 14, Nombre = "Nuevo" },
            //     new Entidades.Control() { ControlId = 17, PaginaId = 14, Nombre = "Ver" },
            //     new Entidades.Control() { ControlId = 18, PaginaId = 15, Nombre = "Grabar" },

            //     new Entidades.Control() { ControlId = 19, PaginaId = 16, Nombre = "Nuevo" },
            //     new Entidades.Control() { ControlId = 20, PaginaId = 16, Nombre = "Ver" },
            //     new Entidades.Control() { ControlId = 21, PaginaId = 17, Nombre = "Grabar" },

            //     new Entidades.Control() { ControlId = 22, PaginaId = 18, Nombre = "Nuevo" },
            //     new Entidades.Control() { ControlId = 23, PaginaId = 18, Nombre = "Ver" },
            //     new Entidades.Control() { ControlId = 24, PaginaId = 19, Nombre = "Grabar" }

            // );

            // context.Rol.AddOrUpdate(
            //     new Entidades.Rol() { RolId = 1, Nombre = "Administrador", Estado = Entidades.EstadoRol.Activo }
            // );
            // context.RolMenu.AddOrUpdate(
            //     new Entidades.RolMenu() { RolId = 1, MenuId = 1 },
            //     new Entidades.RolMenu() { RolId = 1, MenuId = 2 },
            //     new Entidades.RolMenu() { RolId = 1, MenuId = 3 }
            // );
            // context.RolPagina.AddOrUpdate(
            //     new Entidades.RolPagina() { RolId = 1, PaginaId = 1 },
            //     new Entidades.RolPagina() { RolId = 1, PaginaId = 2 },
            //     new Entidades.RolPagina() { RolId = 1, PaginaId = 3 },
            //     new Entidades.RolPagina() { RolId = 1, PaginaId = 4 }
            // );
            // context.RolControl.AddOrUpdate(
            //     new Entidades.RolControl() { RolId = 1, ControlId = 1 },
            //     new Entidades.RolControl() { RolId = 1, ControlId = 2 },
            //     new Entidades.RolControl() { RolId = 1, ControlId = 3 },
            //     new Entidades.RolControl() { RolId = 1, ControlId = 4 },
            //     new Entidades.RolControl() { RolId = 1, ControlId = 5 },
            //     new Entidades.RolControl() { RolId = 1, ControlId = 6 }
            // );
            // context.Empleado.AddOrUpdate(
            //     new Entidades.Empleado() { EmpleadoId = 1, Nombre = "Junior", Apellido = "Sulca", Celular = null, Telefono = null, Correo = "junior.sulca@outlook.com", Clave = Helpers.CryptographyHelper.Encrypt("Sulca@8524"), Estado = Entidades.EstadoUsuario.Activo, TipoDocumento = Entidades.TipoDocumento.DNI, NroDocumento = "70025043", RolId = 1 }
            // );

            // context.Configuracion.AddOrUpdate(
            //     new Entidades.Configuracion() { ConfiguracionId = 1, CorrelativoOrdenVenta = (context.OrdenVenta.Count() + 1) }
            // );

        }
    }
}
