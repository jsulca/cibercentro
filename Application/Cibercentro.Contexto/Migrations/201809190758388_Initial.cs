namespace Cibercentro.Contexto.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "seg.Control",
                c => new
                    {
                        ControlId = c.Int(nullable: false, identity: true),
                        PaginaId = c.Int(nullable: false),
                        Nombre = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.ControlId)
                .ForeignKey("seg.Pagina", t => t.PaginaId, cascadeDelete: true)
                .Index(t => t.PaginaId);
            
            CreateTable(
                "seg.Pagina",
                c => new
                    {
                        PaginaId = c.Int(nullable: false, identity: true),
                        Nombre = c.String(nullable: false, maxLength: 100),
                        Area = c.String(nullable: false, maxLength: 100),
                        Controlador = c.String(nullable: false, maxLength: 100),
                        Accion = c.String(nullable: false, maxLength: 100),
                        Estado = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.PaginaId);
            
            CreateTable(
                "seg.RolPagina",
                c => new
                    {
                        RolId = c.Int(nullable: false),
                        PaginaId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.RolId, t.PaginaId })
                .ForeignKey("seg.Pagina", t => t.PaginaId, cascadeDelete: true)
                .ForeignKey("seg.Rol", t => t.RolId, cascadeDelete: true)
                .Index(t => t.RolId)
                .Index(t => t.PaginaId);
            
            CreateTable(
                "seg.Rol",
                c => new
                    {
                        RolId = c.Int(nullable: false, identity: true),
                        Nombre = c.String(nullable: false, maxLength: 100),
                        Estado = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.RolId);
            
            CreateTable(
                "seg.RolControl",
                c => new
                    {
                        RolId = c.Int(nullable: false),
                        ControlId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.RolId, t.ControlId })
                .ForeignKey("seg.Control", t => t.ControlId, cascadeDelete: true)
                .ForeignKey("seg.Rol", t => t.RolId, cascadeDelete: true)
                .Index(t => t.RolId)
                .Index(t => t.ControlId);
            
            CreateTable(
                "seg.Empleado",
                c => new
                    {
                        EmpleadoId = c.Int(nullable: false, identity: true),
                        RolId = c.Int(nullable: false),
                        Clave = c.String(nullable: false, maxLength: 100),
                        Estado = c.Int(nullable: false),
                        Nombre = c.String(nullable: false, maxLength: 100),
                        Apellido = c.String(nullable: false, maxLength: 100),
                        TipoDocumento = c.Int(nullable: false),
                        NroDocumento = c.String(nullable: false, maxLength: 30),
                        Correo = c.String(maxLength: 50),
                        Telefono = c.String(maxLength: 20),
                        Celular = c.String(maxLength: 20),
                    })
                .PrimaryKey(t => t.EmpleadoId)
                .ForeignKey("seg.Rol", t => t.RolId, cascadeDelete: true)
                .Index(t => t.RolId);
            
            CreateTable(
                "seg.RolMenu",
                c => new
                    {
                        RolId = c.Int(nullable: false),
                        MenuId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.RolId, t.MenuId })
                .ForeignKey("seg.Menu", t => t.MenuId, cascadeDelete: true)
                .ForeignKey("seg.Rol", t => t.RolId, cascadeDelete: true)
                .Index(t => t.RolId)
                .Index(t => t.MenuId);
            
            CreateTable(
                "seg.Menu",
                c => new
                    {
                        MenuId = c.Int(nullable: false, identity: true),
                        PadreId = c.Int(nullable: false),
                        Nombre = c.String(nullable: false, maxLength: 50),
                        Url = c.String(maxLength: 50),
                        Icono = c.String(maxLength: 50),
                        Tipo = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.MenuId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("seg.Control", "PaginaId", "seg.Pagina");
            DropForeignKey("seg.RolPagina", "RolId", "seg.Rol");
            DropForeignKey("seg.RolMenu", "RolId", "seg.Rol");
            DropForeignKey("seg.RolMenu", "MenuId", "seg.Menu");
            DropForeignKey("seg.Empleado", "RolId", "seg.Rol");
            DropForeignKey("seg.RolControl", "RolId", "seg.Rol");
            DropForeignKey("seg.RolControl", "ControlId", "seg.Control");
            DropForeignKey("seg.RolPagina", "PaginaId", "seg.Pagina");
            DropIndex("seg.RolMenu", new[] { "MenuId" });
            DropIndex("seg.RolMenu", new[] { "RolId" });
            DropIndex("seg.Empleado", new[] { "RolId" });
            DropIndex("seg.RolControl", new[] { "ControlId" });
            DropIndex("seg.RolControl", new[] { "RolId" });
            DropIndex("seg.RolPagina", new[] { "PaginaId" });
            DropIndex("seg.RolPagina", new[] { "RolId" });
            DropIndex("seg.Control", new[] { "PaginaId" });
            DropTable("seg.Menu");
            DropTable("seg.RolMenu");
            DropTable("seg.Empleado");
            DropTable("seg.RolControl");
            DropTable("seg.Rol");
            DropTable("seg.RolPagina");
            DropTable("seg.Pagina");
            DropTable("seg.Control");
        }
    }
}
