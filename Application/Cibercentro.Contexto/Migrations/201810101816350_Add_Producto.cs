namespace Cibercentro.Contexto.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_Producto : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "gen.Producto",
                c => new
                    {
                        Codigo = c.String(nullable: false, maxLength: 50),
                        Nombre = c.String(nullable: false, maxLength: 200),
                        Precio = c.Decimal(nullable: false, precision: 20, scale: 2),
                    })
                .PrimaryKey(t => t.Codigo);
            
        }
        
        public override void Down()
        {
            DropTable("gen.Producto");
        }
    }
}
