namespace Cibercentro.Contexto.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update_OrdenVenta_3 : DbMigration
    {
        public override void Up()
        {
            AddColumn("ven.OrdenVenta", "Estado", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("ven.OrdenVenta", "Estado");
        }
    }
}
