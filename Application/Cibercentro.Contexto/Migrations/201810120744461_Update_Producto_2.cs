namespace Cibercentro.Contexto.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update_Producto_2 : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("gen.Producto");
            AddColumn("gen.Producto", "ProductoId", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("gen.Producto", "ProductoId");
        }
        
        public override void Down()
        {
            DropPrimaryKey("gen.Producto");
            DropColumn("gen.Producto", "ProductoId");
            AddPrimaryKey("gen.Producto", "Codigo");
        }
    }
}
