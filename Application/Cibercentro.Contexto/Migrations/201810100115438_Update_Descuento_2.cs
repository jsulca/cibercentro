namespace Cibercentro.Contexto.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update_Descuento_2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("mov.Descuento", "Estado", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("mov.Descuento", "Estado");
        }
    }
}
