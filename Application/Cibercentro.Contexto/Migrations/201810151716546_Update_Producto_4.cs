namespace Cibercentro.Contexto.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update_Producto_4 : DbMigration
    {
        public override void Up()
        {
            AddColumn("gen.Producto", "Stock", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("gen.Producto", "Stock");
        }
    }
}
