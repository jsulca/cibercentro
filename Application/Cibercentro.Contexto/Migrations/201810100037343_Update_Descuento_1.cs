namespace Cibercentro.Contexto.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update_Descuento_1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("mov.Descuento", "Fecha", c => c.DateTime(nullable: false, storeType: "date"));
        }
        
        public override void Down()
        {
            DropColumn("mov.Descuento", "Fecha");
        }
    }
}
