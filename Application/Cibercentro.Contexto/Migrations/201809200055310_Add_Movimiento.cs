namespace Cibercentro.Contexto.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_Movimiento : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "mov.Egreso",
                c => new
                    {
                        EgresoId = c.Int(nullable: false, identity: true),
                        EmpleadoId = c.Int(nullable: false),
                        EmpleadoCreacionId = c.Int(nullable: false),
                        EmpleadoActualizacionId = c.Int(),
                        FechaCreacion = c.DateTime(nullable: false),
                        FechaActualizacion = c.DateTime(),
                        Cantidad = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Motivo = c.String(nullable: false, maxLength: 200),
                        Observacion = c.String(maxLength: 2000),
                    })
                .PrimaryKey(t => t.EgresoId);
            
            CreateTable(
                "mov.Ingreso",
                c => new
                    {
                        IngresoId = c.Int(nullable: false, identity: true),
                        EmpleadoId = c.Int(nullable: false),
                        EmpleadoCreacionId = c.Int(nullable: false),
                        EmpleadoActualizacionId = c.Int(),
                        FechaCreacion = c.DateTime(nullable: false),
                        FechaActualizacion = c.DateTime(),
                        Cantidad = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Observacion = c.String(maxLength: 2000),
                    })
                .PrimaryKey(t => t.IngresoId);
            
        }
        
        public override void Down()
        {
            DropTable("mov.Ingreso");
            DropTable("mov.Egreso");
        }
    }
}
