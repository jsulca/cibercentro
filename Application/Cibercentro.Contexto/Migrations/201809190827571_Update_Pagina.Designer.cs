// <auto-generated />
namespace Cibercentro.Contexto.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class Update_Pagina : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Update_Pagina));
        
        string IMigrationMetadata.Id
        {
            get { return "201809190827571_Update_Pagina"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
