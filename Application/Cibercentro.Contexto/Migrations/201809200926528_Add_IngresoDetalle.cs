namespace Cibercentro.Contexto.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_IngresoDetalle : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "mov.IngresoDetalle",
                c => new
                    {
                        IngresoId = c.Int(nullable: false),
                        TipoIngresoId = c.Int(nullable: false),
                        Cantidad = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => new { t.IngresoId, t.TipoIngresoId })
                .ForeignKey("mov.Ingreso", t => t.IngresoId, cascadeDelete: true)
                .ForeignKey("gen.TipoIngreso", t => t.TipoIngresoId, cascadeDelete: true)
                .Index(t => t.IngresoId)
                .Index(t => t.TipoIngresoId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("mov.IngresoDetalle", "TipoIngresoId", "gen.TipoIngreso");
            DropForeignKey("mov.IngresoDetalle", "IngresoId", "mov.Ingreso");
            DropIndex("mov.IngresoDetalle", new[] { "TipoIngresoId" });
            DropIndex("mov.IngresoDetalle", new[] { "IngresoId" });
            DropTable("mov.IngresoDetalle");
        }
    }
}
