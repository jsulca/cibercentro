namespace Cibercentro.Contexto.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update_Egreso_1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("mov.Egreso", "Tipo", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("mov.Egreso", "Tipo");
        }
    }
}
