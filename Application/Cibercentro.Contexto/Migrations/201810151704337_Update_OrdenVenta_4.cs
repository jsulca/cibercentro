namespace Cibercentro.Contexto.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update_OrdenVenta_4 : DbMigration
    {
        public override void Up()
        {
            AddColumn("ven.OrdenVenta", "EmpleadoAnulacionId", c => c.Int());
            AddColumn("ven.OrdenVenta", "FechaAnulacion", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("ven.OrdenVenta", "FechaAnulacion");
            DropColumn("ven.OrdenVenta", "EmpleadoAnulacionId");
        }
    }
}
