namespace Cibercentro.Contexto.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_Configuracion : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "gen.Configuracion",
                c => new
                    {
                        ConfiguracionId = c.Int(nullable: false),
                        CorrelativoOrdenVenta = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ConfiguracionId);
            
        }
        
        public override void Down()
        {
            DropTable("gen.Configuracion");
        }
    }
}
