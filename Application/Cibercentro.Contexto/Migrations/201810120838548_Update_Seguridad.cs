namespace Cibercentro.Contexto.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update_Seguridad : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("seg.RolControl", "ControlId", "seg.Control");
            DropForeignKey("seg.RolPagina", "PaginaId", "seg.Pagina");
            DropForeignKey("seg.Control", "PaginaId", "seg.Pagina");
            DropForeignKey("seg.RolMenu", "MenuId", "seg.Menu");
            DropPrimaryKey("seg.Control");
            DropPrimaryKey("seg.Pagina");
            DropPrimaryKey("seg.Menu");
            AlterColumn("seg.Control", "ControlId", c => c.Int(nullable: false));
            AlterColumn("seg.Pagina", "PaginaId", c => c.Int(nullable: false));
            AlterColumn("seg.Menu", "MenuId", c => c.Int(nullable: false));
            AddPrimaryKey("seg.Control", "ControlId");
            AddPrimaryKey("seg.Pagina", "PaginaId");
            AddPrimaryKey("seg.Menu", "MenuId");
            AddForeignKey("seg.RolControl", "ControlId", "seg.Control", "ControlId", cascadeDelete: true);
            AddForeignKey("seg.RolPagina", "PaginaId", "seg.Pagina", "PaginaId", cascadeDelete: true);
            AddForeignKey("seg.Control", "PaginaId", "seg.Pagina", "PaginaId", cascadeDelete: true);
            AddForeignKey("seg.RolMenu", "MenuId", "seg.Menu", "MenuId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("seg.RolMenu", "MenuId", "seg.Menu");
            DropForeignKey("seg.Control", "PaginaId", "seg.Pagina");
            DropForeignKey("seg.RolPagina", "PaginaId", "seg.Pagina");
            DropForeignKey("seg.RolControl", "ControlId", "seg.Control");
            DropPrimaryKey("seg.Menu");
            DropPrimaryKey("seg.Pagina");
            DropPrimaryKey("seg.Control");
            AlterColumn("seg.Menu", "MenuId", c => c.Int(nullable: false, identity: true));
            AlterColumn("seg.Pagina", "PaginaId", c => c.Int(nullable: false, identity: true));
            AlterColumn("seg.Control", "ControlId", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("seg.Menu", "MenuId");
            AddPrimaryKey("seg.Pagina", "PaginaId");
            AddPrimaryKey("seg.Control", "ControlId");
            AddForeignKey("seg.RolMenu", "MenuId", "seg.Menu", "MenuId", cascadeDelete: true);
            AddForeignKey("seg.Control", "PaginaId", "seg.Pagina", "PaginaId", cascadeDelete: true);
            AddForeignKey("seg.RolPagina", "PaginaId", "seg.Pagina", "PaginaId", cascadeDelete: true);
            AddForeignKey("seg.RolControl", "ControlId", "seg.Control", "ControlId", cascadeDelete: true);
        }
    }
}
