namespace Cibercentro.Contexto.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update_Pagina : DbMigration
    {
        public override void Up()
        {
            DropColumn("seg.Pagina", "Area");
        }
        
        public override void Down()
        {
            AddColumn("seg.Pagina", "Area", c => c.String(nullable: false, maxLength: 100));
        }
    }
}
