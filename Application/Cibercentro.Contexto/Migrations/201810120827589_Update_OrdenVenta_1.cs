namespace Cibercentro.Contexto.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update_OrdenVenta_1 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("ven.OrdenVentaDetalle", "CodigoOrdenVenta", "ven.OrdenVenta");
            DropIndex("ven.OrdenVentaDetalle", new[] { "CodigoOrdenVenta" });
            DropPrimaryKey("ven.OrdenVenta");
            DropPrimaryKey("ven.OrdenVentaDetalle");
            AlterColumn("ven.OrdenVenta", "Codigo", c => c.String(nullable: false, maxLength: 20));
            AlterColumn("ven.OrdenVentaDetalle", "CodigoOrdenVenta", c => c.String(nullable: false, maxLength: 20));
            AddPrimaryKey("ven.OrdenVenta", "Codigo");
            AddPrimaryKey("ven.OrdenVentaDetalle", new[] { "CodigoOrdenVenta", "ProductoId" });
            CreateIndex("ven.OrdenVentaDetalle", "CodigoOrdenVenta");
            AddForeignKey("ven.OrdenVentaDetalle", "CodigoOrdenVenta", "ven.OrdenVenta", "Codigo", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("ven.OrdenVentaDetalle", "CodigoOrdenVenta", "ven.OrdenVenta");
            DropIndex("ven.OrdenVentaDetalle", new[] { "CodigoOrdenVenta" });
            DropPrimaryKey("ven.OrdenVentaDetalle");
            DropPrimaryKey("ven.OrdenVenta");
            AlterColumn("ven.OrdenVentaDetalle", "CodigoOrdenVenta", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("ven.OrdenVenta", "Codigo", c => c.String(nullable: false, maxLength: 128));
            AddPrimaryKey("ven.OrdenVentaDetalle", new[] { "CodigoOrdenVenta", "ProductoId" });
            AddPrimaryKey("ven.OrdenVenta", "Codigo");
            CreateIndex("ven.OrdenVentaDetalle", "CodigoOrdenVenta");
            AddForeignKey("ven.OrdenVentaDetalle", "CodigoOrdenVenta", "ven.OrdenVenta", "Codigo", cascadeDelete: true);
        }
    }
}
