namespace Cibercentro.Contexto.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update_OrdenVenta_2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("ven.OrdenVenta", "EmpleadoCreacionId", c => c.Int(nullable: false));
            DropColumn("ven.OrdenVenta", "UsuarioCreacionId");
        }
        
        public override void Down()
        {
            AddColumn("ven.OrdenVenta", "UsuarioCreacionId", c => c.Int(nullable: false));
            DropColumn("ven.OrdenVenta", "EmpleadoCreacionId");
        }
    }
}
