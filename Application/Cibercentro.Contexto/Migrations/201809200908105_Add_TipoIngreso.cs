namespace Cibercentro.Contexto.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_TipoIngreso : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "gen.TipoIngreso",
                c => new
                    {
                        TipoIngresoId = c.Int(nullable: false, identity: true),
                        Nombre = c.String(nullable: false, maxLength: 50),
                        Estado = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.TipoIngresoId);
            
        }
        
        public override void Down()
        {
            DropTable("gen.TipoIngreso");
        }
    }
}
