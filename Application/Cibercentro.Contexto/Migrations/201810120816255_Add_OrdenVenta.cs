namespace Cibercentro.Contexto.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_OrdenVenta : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "ven.OrdenVenta",
                c => new
                    {
                        Codigo = c.String(nullable: false, maxLength: 128),
                        FechaCreacion = c.DateTime(nullable: false),
                        UsuarioCreacionId = c.Int(nullable: false),
                        Total = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Codigo);
            
            CreateTable(
                "ven.OrdenVentaDetalle",
                c => new
                    {
                        CodigoOrdenVenta = c.String(nullable: false, maxLength: 128),
                        ProductoId = c.Int(nullable: false),
                        Precio = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Cantidad = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.CodigoOrdenVenta, t.ProductoId })
                .ForeignKey("ven.OrdenVenta", t => t.CodigoOrdenVenta, cascadeDelete: true)
                .ForeignKey("gen.Producto", t => t.ProductoId, cascadeDelete: true)
                .Index(t => t.CodigoOrdenVenta)
                .Index(t => t.ProductoId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("ven.OrdenVentaDetalle", "ProductoId", "gen.Producto");
            DropForeignKey("ven.OrdenVentaDetalle", "CodigoOrdenVenta", "ven.OrdenVenta");
            DropIndex("ven.OrdenVentaDetalle", new[] { "ProductoId" });
            DropIndex("ven.OrdenVentaDetalle", new[] { "CodigoOrdenVenta" });
            DropTable("ven.OrdenVentaDetalle");
            DropTable("ven.OrdenVenta");
        }
    }
}
