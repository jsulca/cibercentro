namespace Cibercentro.Contexto.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update_Producto_1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("gen.Producto", "EmpleadoCreacionId", c => c.Int(nullable: false));
            AddColumn("gen.Producto", "EmpleadoActualizacionId", c => c.Int());
            AddColumn("gen.Producto", "FechaCreacion", c => c.DateTime(nullable: false));
            AddColumn("gen.Producto", "FechaActualizacion", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("gen.Producto", "FechaActualizacion");
            DropColumn("gen.Producto", "FechaCreacion");
            DropColumn("gen.Producto", "EmpleadoActualizacionId");
            DropColumn("gen.Producto", "EmpleadoCreacionId");
        }
    }
}
