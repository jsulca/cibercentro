namespace Cibercentro.Contexto.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update_Movimiento : DbMigration
    {
        public override void Up()
        {
            AddColumn("mov.Egreso", "FechaRegistro", c => c.DateTime(nullable: false, storeType: "date"));
            AddColumn("mov.Ingreso", "FechaRegistro", c => c.DateTime(nullable: false, storeType: "date"));
        }
        
        public override void Down()
        {
            DropColumn("mov.Ingreso", "FechaRegistro");
            DropColumn("mov.Egreso", "FechaRegistro");
        }
    }
}
