namespace Cibercentro.Contexto.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_Pago : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "mov.PagoDescuento",
                c => new
                    {
                        PagoId = c.Int(nullable: false),
                        DescuentoId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.PagoId, t.DescuentoId })
                .ForeignKey("mov.Descuento", t => t.DescuentoId, cascadeDelete: true)
                .ForeignKey("mov.Pago", t => t.PagoId, cascadeDelete: true)
                .Index(t => t.PagoId)
                .Index(t => t.DescuentoId);
            
            CreateTable(
                "mov.Pago",
                c => new
                    {
                        PagoId = c.Int(nullable: false, identity: true),
                        Desde = c.DateTime(nullable: false, storeType: "date"),
                        Hasta = c.DateTime(nullable: false, storeType: "date"),
                        EmpleadoId = c.Int(nullable: false),
                        EmpleadoCreacionId = c.Int(nullable: false),
                        FechaCreacion = c.DateTime(nullable: false),
                        SubTotal = c.Decimal(nullable: false, precision: 12, scale: 2),
                        TotalDescuento = c.Decimal(nullable: false, precision: 12, scale: 2),
                        Total = c.Decimal(nullable: false, precision: 12, scale: 2),
                    })
                .PrimaryKey(t => t.PagoId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("mov.PagoDescuento", "PagoId", "mov.Pago");
            DropForeignKey("mov.PagoDescuento", "DescuentoId", "mov.Descuento");
            DropIndex("mov.PagoDescuento", new[] { "DescuentoId" });
            DropIndex("mov.PagoDescuento", new[] { "PagoId" });
            DropTable("mov.Pago");
            DropTable("mov.PagoDescuento");
        }
    }
}
