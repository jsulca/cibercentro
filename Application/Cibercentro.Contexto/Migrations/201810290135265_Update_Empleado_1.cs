namespace Cibercentro.Contexto.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update_Empleado_1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("seg.Empleado", "Sueldo", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("seg.Empleado", "Sueldo");
        }
    }
}
