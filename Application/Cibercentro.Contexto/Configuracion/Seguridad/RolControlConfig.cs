﻿using Cibercentro.Entidades;
using System.Data.Entity.ModelConfiguration;

namespace Cibercentro.Contexto.Configuracion
{
    public class RolControlConfig
    {
        public RolControlConfig(EntityTypeConfiguration<RolControl> modelBuilder)
        {
            modelBuilder.ToTable("RolControl", "seg").HasKey(x => new { x.RolId, x.ControlId });

            modelBuilder.HasRequired(x => x.Rol).WithMany(x => x.Controles).HasForeignKey(x => x.RolId);
            modelBuilder.HasRequired(x => x.Control).WithMany(x => x.RolControl).HasForeignKey(x => x.ControlId);
        }
    }
}
