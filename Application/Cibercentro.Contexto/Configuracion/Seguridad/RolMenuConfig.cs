﻿using Cibercentro.Entidades;
using System.Data.Entity.ModelConfiguration;

namespace Cibercentro.Contexto.Configuracion
{
    public class RolMenuConfig
    {
        public RolMenuConfig(EntityTypeConfiguration<RolMenu> modelBuilder)
        {
            modelBuilder.ToTable("RolMenu", "seg").HasKey(x => new { x.RolId, x.MenuId });

            modelBuilder.HasRequired(x => x.Rol).WithMany(x => x.Menus).HasForeignKey(x => x.RolId);
            modelBuilder.HasRequired(x => x.Menu).WithMany(x => x.RolMenu).HasForeignKey(x => x.MenuId);
        }
    }
}
