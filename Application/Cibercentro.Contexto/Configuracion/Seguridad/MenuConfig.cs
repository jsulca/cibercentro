﻿using Cibercentro.Entidades;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cibercentro.Contexto.Configuracion
{
    public class MenuConfig
    {
        public MenuConfig(EntityTypeConfiguration<Menu> modelBuilder)
        {
            modelBuilder.ToTable("Menu", "seg").HasKey(x => x.MenuId);

            modelBuilder.Property(x => x.MenuId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            modelBuilder.Property(x => x.Nombre).HasMaxLength(50).IsRequired();
            modelBuilder.Property(x => x.Url).HasMaxLength(50);
            modelBuilder.Property(x => x.Icono).HasMaxLength(50);
            modelBuilder.Property(x => x.Tipo).IsRequired();
        }
    }
}
