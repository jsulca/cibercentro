﻿using Cibercentro.Entidades;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cibercentro.Contexto.Configuracion
{
    public class PaginaConfig
    {
        public PaginaConfig(EntityTypeConfiguration<Pagina> modelBuilder)
        {
            modelBuilder.ToTable("Pagina", "seg").HasKey(x => x.PaginaId);
            modelBuilder.Property(x => x.PaginaId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            modelBuilder.Property(x => x.Nombre).HasMaxLength(100).IsRequired();
            modelBuilder.Property(x => x.Controlador).HasMaxLength(100).IsRequired();
            modelBuilder.Property(x => x.Accion).HasMaxLength(100).IsRequired();
        }
    }
}
