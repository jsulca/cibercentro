﻿using Cibercentro.Entidades;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cibercentro.Contexto.Configuracion
{
    public class EmpleadoConfig
    {
        public EmpleadoConfig(EntityTypeConfiguration<Empleado> modelBuilder)
        {
            modelBuilder.ToTable("Empleado", "seg").HasKey(x => x.EmpleadoId);

            modelBuilder.Property(x => x.EmpleadoId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Property(x => x.Clave).HasMaxLength(100).IsRequired();
            modelBuilder.Property(x => x.Nombre).HasMaxLength(100).IsRequired();
            modelBuilder.Property(x => x.Apellido).HasMaxLength(100).IsRequired();
            modelBuilder.Property(x => x.NroDocumento).HasMaxLength(30).IsRequired();
            modelBuilder.Property(x => x.Correo).HasMaxLength(50);
            modelBuilder.Property(x => x.Telefono).HasMaxLength(20);
            modelBuilder.Property(x => x.Celular).HasMaxLength(20);
            
            modelBuilder.HasRequired(x => x.Rol).WithMany(x => x.Empleados).HasForeignKey(x => x.RolId);
        }
    }
}
