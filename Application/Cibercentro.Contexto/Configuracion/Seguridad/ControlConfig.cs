﻿using Cibercentro.Entidades;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cibercentro.Contexto.Configuracion
{
    public class ControlConfig
    {
        public ControlConfig(EntityTypeConfiguration<Control> modelBuilder)
        {
            modelBuilder.ToTable("Control", "seg").HasKey(x => x.ControlId);

            modelBuilder.Property(x => x.ControlId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            modelBuilder.Property(x => x.Nombre).HasMaxLength(50).IsRequired();
            modelBuilder.HasRequired(x => x.Pagina).WithMany(x => x.Controles).HasForeignKey(x => x.PaginaId);

            modelBuilder.Ignore(x => x.Eliminado);
        }
    }
}
