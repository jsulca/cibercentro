﻿using Cibercentro.Entidades;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cibercentro.Contexto.Configuracion
{
    public class RolConfig
    {
        public RolConfig(EntityTypeConfiguration<Rol> modelBuilder)
        {
            modelBuilder.ToTable("Rol", "seg").HasKey(x => x.RolId);

            modelBuilder.Property(x => x.RolId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Property(x => x.Nombre).HasMaxLength(100).IsRequired();
        }
    }
}
