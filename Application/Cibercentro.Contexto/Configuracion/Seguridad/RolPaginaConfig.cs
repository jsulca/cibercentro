﻿using Cibercentro.Entidades;
using System.Data.Entity.ModelConfiguration;

namespace Cibercentro.Contexto.Configuracion
{
    public class RolPaginaConfig
    {
        public RolPaginaConfig(EntityTypeConfiguration<RolPagina> modelBuilder)
        {
            modelBuilder.ToTable("RolPagina", "seg").HasKey(x => new { x.RolId, x.PaginaId });

            modelBuilder.HasRequired(x => x.Rol).WithMany(x => x.Paginas).HasForeignKey(x => x.RolId);
            modelBuilder.HasRequired(x => x.Pagina).WithMany(x => x.RolPagina).HasForeignKey(x => x.PaginaId);
        }
    }
}
