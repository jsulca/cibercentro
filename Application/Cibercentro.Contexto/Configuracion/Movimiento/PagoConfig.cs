﻿using Cibercentro.Entidades;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cibercentro.Contexto.Configuracion
{
    public class PagoConfig
    {
        public PagoConfig(EntityTypeConfiguration<Pago> modelBuilder)
        {
            modelBuilder.ToTable("Pago", "mov").HasKey(x => x.PagoId);
            modelBuilder.Property(x => x.PagoId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Property(x => x.Desde).HasColumnType("date");
            modelBuilder.Property(x => x.Hasta).HasColumnType("date");
            modelBuilder.Property(x => x.TotalDescuento).HasPrecision(12, 2);
            modelBuilder.Property(x => x.SubTotal).HasPrecision(12, 2);
            modelBuilder.Property(x => x.Total).HasPrecision(12, 2);
        }
    }
}
