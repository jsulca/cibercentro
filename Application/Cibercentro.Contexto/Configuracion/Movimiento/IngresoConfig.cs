﻿using Cibercentro.Entidades;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cibercentro.Contexto.Configuracion
{
    public class IngresoConfig
    {
        public IngresoConfig(EntityTypeConfiguration<Ingreso> modelBuilder)
        {
            modelBuilder.ToTable("Ingreso", "mov").HasKey(x => x.IngresoId);
            modelBuilder.Property(x => x.IngresoId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Property(x => x.Observacion).HasMaxLength(2000);
            modelBuilder.Property(x => x.FechaRegistro).HasColumnType("date");
            modelBuilder.Ignore(x => x.EmpleadoNombreCompleto);
        }
    }
}
