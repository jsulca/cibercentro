﻿using Cibercentro.Entidades;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cibercentro.Contexto.Configuracion
{
    public class EgresoConfig
    {
        public EgresoConfig(EntityTypeConfiguration<Egreso> modelBuilder)
        {
            modelBuilder.ToTable("Egreso", "mov").HasKey(x => x.EgresoId);
            modelBuilder.Property(x => x.EgresoId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Property(x => x.Motivo).HasMaxLength(200).IsRequired();
            modelBuilder.Property(x => x.Observacion).HasMaxLength(2000);
            modelBuilder.Property(x => x.FechaRegistro).HasColumnType("date");
            modelBuilder.Ignore(x => x.EmpleadoNombreCompleto);
        }
    }
}
