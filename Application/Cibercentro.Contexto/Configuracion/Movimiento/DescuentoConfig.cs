﻿using Cibercentro.Entidades;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cibercentro.Contexto.Configuracion
{
    public class DescuentoConfig
    {
        public DescuentoConfig(EntityTypeConfiguration<Descuento> modelBuilder)
        {
            modelBuilder.ToTable("Descuento", "mov").HasKey(x => x.DescuentoId);
            modelBuilder.Property(x => x.DescuentoId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Property(x => x.Motivo).HasMaxLength(2000).IsRequired();
            modelBuilder.Property(x => x.Cantidad).HasPrecision(22, 2);
            modelBuilder.Property(x => x.Fecha).HasColumnType("date");
            modelBuilder.Ignore(x => x.EmpleadoNombreCompleto);
        }
    }
}
