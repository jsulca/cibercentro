﻿using Cibercentro.Entidades;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cibercentro.Contexto.Configuracion
{
    public class PagoDescuentoConfig
    {
        public PagoDescuentoConfig(EntityTypeConfiguration<PagoDescuento> modelBuilder)
        {
            modelBuilder.ToTable("PagoDescuento", "mov").HasKey(x => new { x.PagoId, x.DescuentoId });

            modelBuilder.HasRequired(x => x.Pago).WithMany(x => x.Descuentos).HasForeignKey(x => x.PagoId);
            modelBuilder.HasRequired(x => x.Descuento).WithMany(x => x.Pagos).HasForeignKey(x => x.DescuentoId);
        }
    }
}
