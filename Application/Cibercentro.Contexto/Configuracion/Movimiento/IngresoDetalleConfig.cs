﻿using Cibercentro.Entidades;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cibercentro.Contexto.Configuracion
{
    public class IngresoDetalleConfig
    {
        public IngresoDetalleConfig(EntityTypeConfiguration<IngresoDetalle> modelBuilder)
        {
            modelBuilder.ToTable("IngresoDetalle", "mov").HasKey(x => new { x.IngresoId, x.TipoIngresoId });

            modelBuilder.HasRequired(x => x.Ingreso).WithMany(x => x.Detalles).HasForeignKey(x => x.IngresoId);
            modelBuilder.HasRequired(x => x.TipoIngreso).WithMany(x => x.Ingresos).HasForeignKey(x => x.TipoIngresoId);

        }
    }
}
