﻿using Cibercentro.Entidades;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cibercentro.Contexto.Configuracion
{
    public class ProductoConfig
    {
        public ProductoConfig(EntityTypeConfiguration<Producto> modelBuilder)
        {

            modelBuilder.ToTable("Producto", "gen").HasKey(x => x.ProductoId);
            modelBuilder.Property(x => x.Codigo).HasMaxLength(50);
            modelBuilder.Property(x => x.Nombre).HasMaxLength(200).IsRequired();
            modelBuilder.Property(x => x.Precio).HasPrecision(20, 2);

        }
    }
}
