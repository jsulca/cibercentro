﻿using Cibercentro.Entidades;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cibercentro.Contexto.Configuracion
{
    public class ConfiguracionConfig
    {
        public ConfiguracionConfig(EntityTypeConfiguration<Entidades.Configuracion> modelBuilder)
        {
            modelBuilder.ToTable("Configuracion", "gen").HasKey(x => x.ConfiguracionId);
            modelBuilder.Property(x => x.ConfiguracionId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
        }
    }
}
