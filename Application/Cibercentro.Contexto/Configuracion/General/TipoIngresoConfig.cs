﻿using Cibercentro.Entidades;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cibercentro.Contexto.Configuracion
{
    public class TipoIngresoConfig
    {
        public TipoIngresoConfig(EntityTypeConfiguration<TipoIngreso> modelBuilder)
        {
            modelBuilder.ToTable("TipoIngreso", "gen").HasKey(x => x.TipoIngresoId);
            modelBuilder.Property(x => x.TipoIngresoId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Property(x => x.Nombre).HasMaxLength(50).IsRequired();
        }
    }
}
