﻿using Cibercentro.Entidades;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cibercentro.Contexto.Configuracion
{
    public class OrdenVentaConfig
    {
        public OrdenVentaConfig(EntityTypeConfiguration<OrdenVenta> modelBuilder)
        {
            modelBuilder.ToTable("OrdenVenta", "ven").HasKey(x => x.Codigo);
            modelBuilder.Property(x => x.Codigo).HasMaxLength(20);
            modelBuilder.Property(x => x.Total).HasPrecision(18, 2);
            modelBuilder.Ignore(x => x.EmpleadoCreacionNombreCompleto);
        }
    }
}
