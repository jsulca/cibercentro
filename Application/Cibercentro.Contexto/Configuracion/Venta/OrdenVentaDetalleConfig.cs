﻿using Cibercentro.Entidades;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;


namespace Cibercentro.Contexto.Configuracion
{
    public class OrdenVentaDetalleConfig
    {
        public OrdenVentaDetalleConfig(EntityTypeConfiguration<OrdenVentaDetalle> modelBuilder)
        {
            modelBuilder.ToTable("OrdenVentaDetalle", "ven").HasKey(x => new { x.CodigoOrdenVenta, x.ProductoId });
            modelBuilder.Property(x => x.CodigoOrdenVenta).HasMaxLength(20);
            modelBuilder.Property(x => x.Precio).HasPrecision(18, 2);
            modelBuilder.HasRequired(x => x.OrdenVenta).WithMany(x => x.Detalles).HasForeignKey(x => x.CodigoOrdenVenta);
            modelBuilder.HasRequired(x => x.Producto).WithMany(x => x.Detalles).HasForeignKey(x => x.ProductoId);
        }
    }
}
