﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace Cibercentro.Helpers
{
    public class ProcedureHelper
    {
       private string StrCn { get; set; }

        public ProcedureHelper(string strCn)
        {
            StrCn = strCn;
        }

        public string ExecuteProcedure(string strCommand)
        {
            try
            {
                using (var cn = new MySqlConnection(StrCn))
                {
                    MySqlCommand cmd = new MySqlCommand(strCommand, cn);
                    cn.Open();
                    string data = (string)cmd.ExecuteScalar();
                    return data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
