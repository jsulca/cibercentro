﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cibercentro.Helpers;

namespace Cibercentro.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            ProcedureHelper procedureHelper = new ProcedureHelper("server=localhost;database=cibercentro;uid=root;pwd=123;SslMode=none");
            string data = procedureHelper.ExecuteProcedure("usp_Empleado_CSV");
            Console.WriteLine(data);
            Console.ReadKey();
        }
    }
}
