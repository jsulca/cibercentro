﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Cibercentro.WebApp.Common
{
    public class FiltroSeguridad: ActionFilterAttribute
    {
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            base.OnActionExecuted(filterContext);
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {

            UserInformation user = (UserInformation)filterContext.HttpContext.Session["user"];
            var url = filterContext.RequestContext.HttpContext.Request.Url.ToString();
            var Controller = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;
            var Action = filterContext.ActionDescriptor.ActionName;
            if (user == null)
            {
                filterContext.Result = new RedirectToRouteResult(new System.Web.Routing.RouteValueDictionary(new
                {
                    controller = "Account",
                    action = "Login",
                    area = "",
                    ReturnUrl = url
                }));
            }
            else
            {
                if (!Controller.Equals("Home") && !Controller.Equals("Account"))
                {
                    var page = user.Pages.FirstOrDefault(x => url.Contains(x.Url));
                    if (page != null)
                    {
                        filterContext.Controller.ViewBag.Controls = user.Controles.Where(x => x.PageId == page.PageId).ToList();
                    }
                    else
                    {
                        filterContext.Result = new RedirectToRouteResult(new System.Web.Routing.RouteValueDictionary(new
                        {
                            controller = "Home",
                            action = "AccesoDenegado"
                        }));
                    }
                }
            }
            base.OnActionExecuting(filterContext);
        }

        public override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            base.OnResultExecuted(filterContext);
        }

        public override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            base.OnResultExecuting(filterContext);
        }
    }
}