﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cibercentro.WebApp.Common
{
    public class TreeItem
    {
        public string id { get; set; }
        public string text { get; set; }
        public string icon { get; set; }
        public object data { get; set; }
        public object state { get; set; }
        public object li_attr { get; set; }
        public object a_attr { get; set; }
        public List<TreeItem> children { get; set; }

        public TreeItem()
        {
            data = new object();
            state = new object();
            li_attr = new object();
            a_attr = new object();
            children = new List<TreeItem>();
        }
    }
}