﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cibercentro.WebApp.Common
{

    public class UserInformation
    {
        public int UserId { get; set; }
        public string Name { get; set; }

        public List<PageInformation> Pages { get; set; }
        public List<ControlInformation> Controles { get; set; }
        public List<MenuInformation> Menus { get; set; }

        public UserInformation()
        {
            Pages = new List<PageInformation>();
            Controles = new List<ControlInformation>();
            Menus = new List<MenuInformation>();
        }
    }

    public class PageInformation
    {
        public int PageId { get; set; }
        public string Url { get; set; }
    }

    public class ControlInformation
    {
        public int PageId { get; set; }
        public string Nombre { get; set; }
    }

    public class MenuInformation
    {
        public int MenuId { get; set; }
        public int PadreId { get; set; }
        public string Icon { get; set; }
        public Entidades.TipoMenu Tipo { get; set; }
        public string Nombre { get; set; }
        public string Url { get; set; }
    }
}