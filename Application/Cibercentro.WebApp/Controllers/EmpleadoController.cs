﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Cibercentro.Helpers;
using Cibercentro.Entidades;
using Cibercentro.Repositorio;
using Newtonsoft.Json;
using Cibercentro.WebApp.Common;
using Cibercentro.Entidades.Filtro;

namespace Cibercentro.WebApp.Controllers
{
    public class EmpleadoController : Controller
    {
        private readonly EmpleadoRepositorio empleadoRepositorio = new EmpleadoRepositorio();

        [FiltroSeguridad]
        public ActionResult Index()
        {
            return View();
        }

        [FiltroSeguridad]
        public ActionResult Editar(int id = 0)
        {
            try
            {
                Empleado model = null;
                if (id > 0)
                {
                    ViewData["Title"] = "Editar empleado";
                    model = empleadoRepositorio.GetOne(id);
                }
                else
                {
                    ViewData["Title"] = "Nuevo empleado";
                    model = new Empleado() { Estado = EstadoUsuario.Activo };
                }

                return View(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public ActionResult Editar(Empleado model)
        {
            try
            {
                ValidEntity(model);
                if (ModelState.IsValid)
                {
                    bool agregar = model.EmpleadoId == 0;
                    int i = empleadoRepositorio.CountBy(x => x.EmpleadoId != model.EmpleadoId && x.TipoDocumento == model.TipoDocumento && x.NroDocumento.Equals(model.NroDocumento));
                    if (i > 0) throw new Exception("Ya existe un usuario con el tipo y nro de documento.");
                    if (agregar) model.Clave = CryptographyHelper.Encrypt(model.Clave);
                    else
                    {
                        Empleado empleado = empleadoRepositorio.GetOne(x => x.EmpleadoId == model.EmpleadoId);
                        model.Clave = string.IsNullOrEmpty(model.Clave) ? empleado.Clave : CryptographyHelper.Encrypt(model.Clave);
                    }
                    empleadoRepositorio.Save(model);
                    return Json(new { mensaje = (agregar ? "El usuario fue registrado." : "El usuario fue editado."), id = model.EmpleadoId });
                }
                else
                {
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    return PartialView("_Error");
                }
            }
            catch (Exception ex)
            {
                ViewData["ErrorMessage"] = ex.Message;
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return PartialView("_Error");
            }
        }

        public ActionResult GetAllLikePagin(EmpleadoFiltro filter, int pageSize, int pageIndex)
        {
            try
            {
                int totalRows = 0;
                List<Empleado> lista = empleadoRepositorio.GetAllLikePagin(filter, pageSize, pageIndex, ref totalRows);
                return Content(JsonConvert.SerializeObject(new
                {
                    lista = lista.Select(x => new
                    {
                        x.EmpleadoId,
                        NRol = x.Rol.Nombre,
                        x.Nombre,
                        x.Apellido,
                        Correo = x.Correo ?? "",
                        TipoDocumento = x.TipoDocumento.ToString(),
                        x.NroDocumento,
                        Estado = x.Estado.ToString()
                    }),
                    totalRows
                }));
            }
            catch (Exception ex)
            {
                ViewData["ErrorMessage"] = ex.Message;
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return PartialView("_Error");
            }
        }

        public ActionResult GetFirst(Empleado filter)
        {
            try
            {
                int totalRows = 0;
                List<Empleado> lista = empleadoRepositorio.GetFirst(filter, SettingsManager.PageSize);
                return Content(JsonConvert.SerializeObject(new
                {
                    lista = lista.Select(x => new
                    {
                        x.EmpleadoId,
                        NombreCompleto = $"{x.Apellido}, {x.Nombre}"
                    }),
                    totalRows
                }));
            }
            catch (Exception ex)
            {
                ViewData["ErrorMessage"] = ex.Message;
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return PartialView("_Error");
            }
        }

        #region Metodos

        private void ValidEntity(Empleado item)
        {
            ModelState.Clear();
            if (item.TipoDocumento <= 0) ModelState.AddModelError("TipoDocumento", "Seleccione un tipo de documento.");
            if (string.IsNullOrEmpty(item.NroDocumento) || string.IsNullOrWhiteSpace(item.NroDocumento)) ModelState.AddModelError("NroDocumento", "Ingrese un nro de documento.");
            if (string.IsNullOrEmpty(item.Nombre) || string.IsNullOrWhiteSpace(item.Nombre)) ModelState.AddModelError("Nombre", "Ingrese un nombre.");
            if (string.IsNullOrEmpty(item.Apellido) || string.IsNullOrWhiteSpace(item.Apellido)) ModelState.AddModelError("Apellido", "Ingrese un apellido.");
            if (item.EmpleadoId == 0 && string.IsNullOrEmpty(item.Clave)) ModelState.AddModelError("Clave", "Ingrese una clave.");
            if (item.Estado <= 0) ModelState.AddModelError("Estado", "Seleccione un estado.");
            if (item.RolId <= 0) ModelState.AddModelError("RolId", "Seleccione un rol.");
        }

        #endregion
    }
}