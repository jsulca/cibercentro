﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Cibercentro.Helpers;
using Cibercentro.Entidades;
using Cibercentro.Repositorio;
using Cibercentro.WebApp.Common;
using Newtonsoft.Json;

namespace Cibercentro.WebApp.Controllers
{
    public class EgresoController : Controller
    {
        private readonly EgresoRepositorio egresoRepositorio = new EgresoRepositorio();

        [FiltroSeguridad]
        public ActionResult Index()
        {
            return View();
        }


        [FiltroSeguridad]
        public ActionResult Editar(int id = 0)
        {
            try
            {
                UserInformation user = (UserInformation)Session["user"];
                Egreso model = null;
                if (id > 0)
                {
                    ViewData["Title"] = "Editar Egreso";
                    model = egresoRepositorio.GetOne(id);
                }
                else
                {
                    ViewData["Title"] = "Nuevo Egreso";
                    model = new Egreso()
                    {
                        EmpleadoId = user.UserId,
                        EmpleadoNombreCompleto = user.Name,
                        FechaRegistro = DateTime.Now
                    };
                }

                return View(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [FiltroSeguridad, HttpPost]
        public ActionResult Editar(Egreso model)
        {
            try
            {
                ValidEntity(model);
                if (ModelState.IsValid)
                {
                    UserInformation user = (UserInformation)Session["user"];

                    bool agregar = model.EgresoId == 0;
                    if (agregar)
                    {
                        model.FechaCreacion = DateTime.Now;
                        model.EmpleadoCreacionId = user.UserId;
                    }
                    else
                    {
                        Egreso egreso = egresoRepositorio.GetOne(model.EgresoId);
                        if (egreso == null) throw new Exception("El egreso que trata de guardar no existe en la base de datos.");
                        egreso.FechaActualizacion = DateTime.Now;
                        egreso.EmpleadoActualizacionId = user.UserId;
                        egreso.FechaRegistro = model.FechaRegistro;
                        egreso.Cantidad = model.Cantidad;
                        egreso.EmpleadoId = model.EmpleadoId;
                        egreso.Motivo = model.Motivo;
                        egreso.Observacion = model.Observacion;
                        egreso.Tipo = model.Tipo;
                        model = egreso;
                    }
                    egresoRepositorio.Save(model);
                    return Json(new { mensaje = (agregar ? "El egreso fue registrado." : "El egreso fue editado."), id = model.EgresoId });
                }
                else
                {
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    return PartialView("_Error");
                }
            }
            catch (Exception ex)
            {
                ViewData["ErrorMessage"] = ex.Message;
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return PartialView("_Error");
            }
        }

        public ActionResult GetAllLikePagin(Egreso filter, int pageSize, int pageIndex)
        {
            try
            {
                int totalRows = 0;
                List<Egreso> lista = egresoRepositorio.GetAllLikePagin(filter, pageSize, pageIndex, ref totalRows);
                return Content(JsonConvert.SerializeObject(new
                {
                    lista = lista.Select(x => new
                    {
                        x.EgresoId,
                        x.EmpleadoNombreCompleto,
                        x.Motivo,
                        FechaRegistro = x.FechaRegistro.ToString("dd/MM/yyyy"),
                        Cantidad = x.Cantidad.ToString("###########0.00"),
                        Tipo = x.Tipo.ToString()
                    }),
                    totalRows
                }));
            }
            catch (Exception ex)
            {
                ViewData["ErrorMessage"] = ex.Message;
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return PartialView("_Error");
            }
        }
        #region Metodos

        public void ValidEntity(Egreso item)
        {
            ModelState.Clear();
            if (item.EmpleadoId <= 0) ModelState.AddModelError("EmpleadoId", "Seleccion un encargado.");
            if (string.IsNullOrEmpty(item.Motivo) || string.IsNullOrWhiteSpace(item.Motivo)) ModelState.AddModelError("Motivo", "Ingrese un motivo.");
            if (item.Cantidad <= 0) ModelState.AddModelError("Cantidad", "La cantidad debe ser mayor a 0."); 
        }

        #endregion
    }
}