﻿using Cibercentro.Entidades;
using Cibercentro.Entidades.Filtro;
using Cibercentro.Repositorio;
using Cibercentro.WebApp.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Cibercentro.WebApp.Controllers
{
    public class ProductoController : Controller
    {
        private readonly ProductoRepositorio productoRepositorio = new ProductoRepositorio();

        [FiltroSeguridad]
        public ActionResult Index()
        {
            return View();
        }
        [FiltroSeguridad]
        public ActionResult Editar(int id = 0)
        {
            try
            {
                Producto model = null;
                if (id == 0)
                {
                    ViewBag.Title = "Nuevo producto";
                    model = new Producto();
                }
                else
                {
                    ViewBag.Title = "Editar producto";
                    model = productoRepositorio.GetOne(x => x.ProductoId == id);
                }

                return View(model);
            }
            catch (Exception ex)
            {
                ViewData["ErrorMessage"] = ex.Message;
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                throw ex;
            }
        }

        [FiltroSeguridad]
        [HttpPost]
        public ActionResult Editar(Producto model)
        {
            try
            {
                ValidEntity(model);
                if (ModelState.IsValid)
                {
                    bool agregar = model.ProductoId == 0;
                    int i = productoRepositorio.CountBy(x => x.ProductoId != model.ProductoId && !string.IsNullOrEmpty(model.Codigo) && x.Codigo.Equals(model.Codigo));
                    if (i > 0) throw new Exception("Ya existe un producto con este codigo.");
                    if (agregar)
                    {
                        model.EmpleadoCreacionId = ((UserInformation)Session["user"]).UserId;
                        model.FechaCreacion = DateTime.Now;
                    }
                    else
                    {
                        Producto producto = productoRepositorio.GetOne(x => x.ProductoId == model.ProductoId);
                        producto.Codigo = model.Codigo;
                        producto.Nombre = model.Nombre;
                        producto.Precio = model.Precio;
                        producto.Stock = model.Stock;
                        producto.EmpleadoActualizacionId = ((UserInformation)Session["user"]).UserId;
                        producto.FechaActualizacion = DateTime.Now;
                        model = producto;
                    }
                    productoRepositorio.Save(model);
                    return Json(new { mensaje = "El producto fue registrado." });
                }
                else
                {
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    return PartialView("_Error");
                }
            }
            catch (Exception ex)
            {
                ViewData["ErrorMessage"] = ex.Message;
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return PartialView("_Error");
            }
        }


        public ActionResult GetAllLikePagin(ProductoFiltro filter, int pageIndex, int pageSize)
        {
            try
            {
                int totalRows = 0;
                List<Producto> lista = productoRepositorio.GetAllLikePagin(filter, pageSize, pageIndex, ref totalRows);
                return Content(JsonConvert.SerializeObject(new
                {
                    lista = lista.Select(x => new
                    {
                        x.ProductoId,
                        x.Codigo,
                        x.Nombre,
                        Precio = x.Precio.ToString("############0.00"),
                        x.Stock
                    }),
                    totalRows
                }));
            }
            catch (Exception ex)
            {
                ViewData["ErrorMessage"] = ex.Message;
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return PartialView("_Error");
            }
        }
        public ActionResult GetAll()
        {

            try
            {
                List<Producto> lista = productoRepositorio.GetAll();
                return Content(JsonConvert.SerializeObject(lista.Select(x => new
                {
                    x.ProductoId,
                    x.Codigo,
                    x.Nombre,
                    x.Precio,
                    x.Stock
                })));
            }
            catch (Exception ex)
            {
                ViewData["ErrorMessage"] = ex.Message;
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return PartialView("_Error");
            }
        }
        #region Metodos

        public void ValidEntity(Producto item)
        {
            ModelState.Clear();
            if (string.IsNullOrEmpty(item.Nombre) || string.IsNullOrWhiteSpace(item.Nombre)) ModelState.AddModelError("Nombre", "Ingrese un nombre.");
            if (item.Precio <= 0) ModelState.AddModelError("Precio", "El precio debe ser mayor o igual que 0.");
        }

        #endregion
    }
}