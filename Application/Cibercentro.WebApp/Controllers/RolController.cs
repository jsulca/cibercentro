﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using Cibercentro.Entidades;
using Cibercentro.Repositorio;
using Cibercentro.WebApp.Common;
using Newtonsoft.Json;

namespace Cibercentro.WebApp.Controllers
{
    public class RolController : Controller
    {
        private readonly RolRepositorio rolRepositorio = new RolRepositorio();
        private readonly MenuRepositorio menuRepositorio = new MenuRepositorio();
        private readonly PaginaRepositorio paginaRepositorio = new PaginaRepositorio();

        [FiltroSeguridad]
        public ActionResult Index()
        {
            return View();
        }
        [FiltroSeguridad]
        public ActionResult Editar(int id = 0)
        {
            try
            {
                Rol model = null;
                if (id > 0)
                {
                    model = rolRepositorio.GetOne(id);
                    ViewData["Title"] = "Editar Rol";
                    ViewData["RolMenu"] = model.Menus != null ? JsonConvert.SerializeObject(model.Menus.Select(x => new { x.MenuId })) : null;
                }
                else
                {
                    model = new Rol() { Estado = EstadoRol.Activo };
                    ViewData["Title"] = "Nuevo Rol";
                }
                TreeItem rootTree = new TreeItem() { id = "0", text = SettingsManager.NombreEmpresa, icon = "fa fa-home", state = new { opened = true } };
                LlenarTreeMenu(rootTree, menuRepositorio.GetAll());
                ViewData["TreeMenu"] = JsonConvert.SerializeObject(rootTree);
                ViewBag.Paginas = paginaRepositorio.GetAllWithControls();
                return View(model);
            }
            catch (Exception ex)
            {
                ViewData["ErrorMessage"] = ex.Message;
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return View("Error");
            }
        }
        [HttpPost]
        public ActionResult Editar(Rol model)
        {
            try
            {
                ValidEntity(model);
                if (ModelState.IsValid)
                {
                    bool agregar = model.RolId == 0;
                    rolRepositorio.Save(model);
                    return Json(new { mensaje = (agregar ? "El rol fue registrado." : "El rol fue editado."), id = model.RolId });
                }
                else
                {
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    return PartialView("_Error");
                }
            }
            catch (Exception ex)
            {
                ViewData["ErrorMessage"] = ex.Message;
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return PartialView("_Error");
            }
        }
        public ActionResult GetAllLikePagin(Rol filter, int pageSize, int pageIndex)
        {
            try
            {
                int totalRows = 0;
                List<Rol> lista = rolRepositorio.GetAllLikePagin(filter, pageSize, pageIndex, ref totalRows);
                return Content(JsonConvert.SerializeObject(new
                {
                    lista = lista.Select(x => new
                    {
                        x.RolId,
                        x.Nombre,
                        Estado = x.Estado.ToString()
                    }),
                    totalRows
                }));
            }
            catch (Exception ex)
            {
                ViewData["ErrorMessage"] = ex.Message;
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return PartialView("_Error");
            }
        }

        #region Metodos

        public void ValidEntity(Rol item)
        {
            ModelState.Clear();
            if (string.IsNullOrEmpty(item.Nombre)) ModelState.AddModelError("Nombre", "Ingrese un nombre.");
        }
        public void LlenarTreeMenu(TreeItem padre, List<Menu> menus)
        {
            var children = menus.Where(x => x.PadreId.ToString().Equals(padre.id)).ToList();
            if (children != null)
            {
                foreach (var child in children)
                {
                    var treeItem = new TreeItem()
                    {
                        id = child.MenuId.ToString(),
                        text = child.Nombre,
                        icon = string.IsNullOrEmpty(child.Icono) ? "false" : child.Icono,
                        state = new { @opened = true },
                        a_attr = new { @class = (child.Tipo == TipoMenu.Header ? "text-primary" : "") }
                    };
                    LlenarTreeMenu(treeItem, menus);
                    padre.children.Add(treeItem);
                }
            }
        }
        public void LlenarTreePagina(TreeItem padre, List<Pagina> paginas)
        {
            foreach (var pagina in paginas.OrderBy(x => x.Controlador))
            {
                TreeItem page = new TreeItem()
                {
                    id = $"p{pagina.PaginaId}",
                    text = $"/{pagina.Controlador}/{pagina.Accion} - {pagina.Nombre}",
                    icon = "fa fa-file-o",
                    state = new { @opened = true },
                    data = new { pagina.PaginaId }
                };
                foreach (var control in pagina.Controles.Where(x => x.PaginaId == pagina.PaginaId))
                {
                    page.children.Add(new TreeItem()
                    {
                        id = $"c{control.ControlId}",
                        text = control.Nombre,
                        icon = "fa fa-cogs",
                        data = new { control.ControlId }
                    });
                }
                padre.children.Add(page);
            }
        }

        #endregion
    }
}