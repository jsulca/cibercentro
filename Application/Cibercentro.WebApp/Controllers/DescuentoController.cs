﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Cibercentro.Helpers;
using Cibercentro.Entidades;
using Cibercentro.Repositorio;
using Cibercentro.WebApp.Common;
using Newtonsoft.Json;

namespace Cibercentro.WebApp.Controllers
{
    public class DescuentoController : Controller
    {
        private readonly DescuentoRepositorio descuentoRepositorio = new DescuentoRepositorio();

        [FiltroSeguridad]
        public ActionResult Index()
        {
            return View();
        }

        [FiltroSeguridad]
        public ActionResult Editar(int id = 0)
        {
            try
            {
                Descuento model = null;
                if (id > 0)
                {
                    ViewData["Title"] = "Editar Descuento";
                    model = descuentoRepositorio.GetOne(id);
                }
                else
                {
                    ViewData["Title"] = "Nuevo Descuento";
                    model = new Descuento() { Fecha = DateTime.Today, Estado = EstadoDescuento.Pendiente };
                }
                return View(model);
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                throw ex;
            }
        }

        [FiltroSeguridad]
        [HttpPost]
        public ActionResult Editar(Descuento model)
        {
            try
            {
                ValidEntity(model);
                if (ModelState.IsValid)
                {
                    bool agregar = model.DescuentoId == 0;
                    int i = descuentoRepositorio.CountBy(x => x.Fecha == model.Fecha && x.EmpleadoId == model.EmpleadoId && x.DescuentoId != model.DescuentoId);
                    if (i > 0) throw new Exception("El encargado ingresado ya tiene un descuento en la misma fecha.");
                    else
                    {
                        if (agregar)
                        {
                            model.EmpleadoCreacionId = ((UserInformation)Session["user"]).UserId;
                            model.FechaCreacion = DateTime.Now;
                            model.Estado = EstadoDescuento.Pendiente;
                        }
                        else
                        {
                            Descuento descuento = descuentoRepositorio.GetOne(x => x.DescuentoId == model.DescuentoId);
                            descuento.EmpleadoId = model.EmpleadoId;
                            descuento.Fecha = model.Fecha;
                            descuento.Motivo = model.Motivo;
                            descuento.Cantidad = model.Cantidad;
                            descuento.FechaActualizacion = DateTime.Now;
                            descuento.EmpleadoActualizacionId = ((UserInformation)Session["user"]).UserId;
                            model = descuento;
                        }
                        descuentoRepositorio.Save(model);
                        return Json(new { mensaje = agregar ? "El descuento fue registrado." : "El descuento fue actualizado", id = model.DescuentoId });
                    }
                }
                else
                {
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    return PartialView("_Error");
                }

            }
            catch (Exception ex)
            {
                ViewData["ErrorMessage"] = ex.Message;
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return PartialView("_Error");
            }
        }

        public ActionResult GetAllLikePagin(Descuento filter, int pageSize, int pageIndex)
        {
            try
            {
                int totalRows = 0;
                List<Descuento> lista = descuentoRepositorio.GetAllLikePagin(filter, pageIndex, pageSize, ref totalRows);
                return Content(JsonConvert.SerializeObject(new
                {
                    lista = lista.Select(x => new
                    {
                        x.DescuentoId,
                        x.EmpleadoNombreCompleto,
                        Cantidad = x.Cantidad.ToString("###########0.00"),
                        x.Motivo,
                        Fecha = x.Fecha.ToString("dd/MM/yyyy"),
                        Estado = x.Estado.ToString(),
                        Editar = x.Estado == EstadoDescuento.Pendiente
                    }),
                    totalRows
                }));
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return PartialView("_Error");
            }
        }

        #region Metodos

        public void ValidEntity(Descuento item)
        {
            ModelState.Clear();
            if (item.EmpleadoId <= 0) ModelState.AddModelError("EmpleadoId", "Seleccion un encargado.");
            if (string.IsNullOrEmpty(item.Motivo) || string.IsNullOrWhiteSpace(item.Motivo)) ModelState.AddModelError("Motivo", "Ingrese un motivo.");
            if (item.Cantidad <= 0) ModelState.AddModelError("Cantidad", "La cantidad debe ser mayor a 0.");
            //if (item.Estado <= 0) ModelState.AddModelError("Estado", "Seleccione un estado.");
        }

        #endregion

    }
}