﻿using Cibercentro.WebApp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Cibercentro.WebApp.Controllers
{
    public class HomeController : Controller
    {
        [FiltroSeguridad]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AccesoDenegado()
        {
            return View();
        }

        public ActionResult UserMenu()
        {
            return PartialView("_UserMenu", ((UserInformation)Session["user"]).Name);
        }
        public ActionResult Menu()
        {
            return PartialView("_Menu", ((UserInformation)Session["user"]).Menus);
        }
    }
}