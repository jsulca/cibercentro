﻿using Cibercentro.Repositorio;
using Cibercentro.Entidades;
using Cibercentro.WebApp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Cibercentro.WebApp.Controllers
{
    public class ReporteController : Controller
    {
        private readonly IngresoRepositorio ingresoRepositorio = new IngresoRepositorio();
        private readonly TipoIngresoRepositorio tipoIngresoRepositorio = new TipoIngresoRepositorio();
        private readonly EgresoRepositorio egresoRepositorio = new EgresoRepositorio();
        private readonly PagoRepositorio pagoRepositorio = new PagoRepositorio();

        [FiltroSeguridad]
        public ActionResult Ingreso(string Desde = null, string Hasta = null)
        {
            DateTime desde = Desde != null ? DateTime.Parse(Desde) : DateTime.Today.AddMonths(-1);
            DateTime hasta = Hasta != null ? DateTime.Parse(Hasta) : DateTime.Today;

            ViewBag.Desde = desde;
            ViewBag.Hasta = hasta;
            List<Ingreso> lista = ingresoRepositorio.GetAllByDates(desde, hasta);
            ViewBag.TipoIngresos = tipoIngresoRepositorio.GetAllBy(x => x.Estado == EstadoTipoIngreso.Habilitado);
            return View(lista);
        }

        [FiltroSeguridad]
        public ActionResult Egreso(string Desde = null, string Hasta = null)
        {
            DateTime desde = Desde != null ? DateTime.Parse(Desde) : DateTime.Today.AddMonths(-1);
            DateTime hasta = Hasta != null ? DateTime.Parse(Hasta) : DateTime.Today;

            ViewBag.Desde = desde;
            ViewBag.Hasta = hasta;
            List<Egreso> lista = egresoRepositorio.GetAllBy(x => x.FechaRegistro >= desde && x.FechaRegistro <= hasta);
            return View(lista);
        }

        [FiltroSeguridad]
        public ActionResult Consolidado(string Desde = null, string Hasta = null)
        {
            DateTime desde = Desde != null ? DateTime.Parse(Desde) : DateTime.Today.AddMonths(-6);
            DateTime hasta = Hasta != null ? DateTime.Parse(Hasta) : DateTime.Today;
            hasta = hasta.AddMinutes(1439);
            ViewBag.Desde = desde;
            ViewBag.Hasta = hasta;
            ViewBag.ListaIngreso = ingresoRepositorio.GetAllBy(x => x.FechaRegistro >= desde && x.FechaRegistro <= hasta);
            ViewBag.ListaEgreso = egresoRepositorio.GetAllBy(x => x.FechaRegistro >= desde && x.FechaRegistro <= hasta);
            ViewBag.ListaPago = pagoRepositorio.GetAllBy(x => x.FechaCreacion >= desde && x.FechaCreacion <= hasta);
            return View();
        }
    }
}