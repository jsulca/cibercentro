﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Cibercentro.Helpers;
using Cibercentro.Entidades;
using Cibercentro.Repositorio;
using Newtonsoft.Json;
using Cibercentro.WebApp.Common;
using Cibercentro.Entidades.Filtro;

namespace Cibercentro.WebApp.Controllers
{
    public class IngresoController : Controller
    {
        private readonly IngresoRepositorio ingresoRepositorio = new IngresoRepositorio();
        private readonly TipoIngresoRepositorio tipoIngresoRepositorio = new TipoIngresoRepositorio();
        private readonly OrdenVentaRepositorio ordenVentaRepositorio = new OrdenVentaRepositorio();

        [FiltroSeguridad]
        public ActionResult Index()
        {
            return View();
        }

        [FiltroSeguridad]
        public ActionResult Editar(int id = 0)
        {
            try
            {
                UserInformation user = (UserInformation)Session["user"];
                Ingreso model = null;
                if (id > 0)
                {
                    ViewData["Title"] = "Editar Ingreso";
                    model = ingresoRepositorio.GetOne(id);
                }
                else
                {
                    ViewData["Title"] = "Nuevo Ingreso";
                    var tiposIngreso = tipoIngresoRepositorio.GetAllBy(x => x.Estado == EstadoTipoIngreso.Habilitado);
                    model = new Ingreso()
                    {
                        EmpleadoId = user.UserId,
                        EmpleadoNombreCompleto = user.Name,
                        FechaRegistro = DateTime.Now,
                        Detalles = tiposIngreso.Select(x => new IngresoDetalle { TipoIngresoId = x.TipoIngresoId, TipoIngreso = x }).ToList()
                    };
                }

                return View(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [FiltroSeguridad, HttpPost]
        public ActionResult Editar(Ingreso model)
        {
            try
            {
                ValidEntity(model);
                if (ModelState.IsValid)
                {
                    UserInformation user = (UserInformation)Session["user"];

                    bool agregar = model.IngresoId == 0;
                    if (agregar)
                    {
                        var i = ingresoRepositorio.CountBy(x => x.EmpleadoId == model.EmpleadoId && x.FechaRegistro == model.FechaRegistro);
                        if (i > 0) throw new Exception("Ya existe un registro ingresado a ese usuario y a esa misma fecha.");
                        model.FechaCreacion = DateTime.Now;
                        model.EmpleadoCreacionId = user.UserId;
                        model.Cantidad = model.Detalles != null ? model.Detalles.Sum(x => x.Cantidad) : 0;
                    }
                    else
                    {
                        Ingreso ingreso = ingresoRepositorio.GetOne(model.IngresoId);
                        if (ingreso == null) throw new Exception("El ingreso que trata de guardar no existe en la base de datos.");
                        ingreso.FechaActualizacion = DateTime.Now;
                        ingreso.EmpleadoActualizacionId = user.UserId;
                        ingreso.FechaRegistro = model.FechaRegistro;
                        ingreso.Cantidad = model.Cantidad;
                        ingreso.EmpleadoId = model.EmpleadoId;
                        ingreso.Observacion = model.Observacion;
                        ingreso.Cantidad = model.Detalles != null ? model.Detalles.Sum(x => x.Cantidad) : 0;
                        ingreso.Detalles = model.Detalles;
                        model = ingreso;
                    }
                    ingresoRepositorio.Save(model);
                    return Json(new { mensaje = (agregar ? "El ingreso fue registrado." : "El ingreso fue editado."), id = model.IngresoId });
                }
                else
                {
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    return PartialView("_Error");
                }
            }
            catch (Exception ex)
            {
                ViewData["ErrorMessage"] = ex.Message;
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return PartialView("_Error");
            }
        }
        
        public ActionResult ValidarMontoTienda(Ingreso model)
        {
            try
            {
                ValidEntity(model);
                if (ModelState.IsValid)
                {
                    DateTime hoy = DateTime.Today;
                    UserInformation user = Session["user"] != null ? (UserInformation)Session["user"] : new UserInformation();
                    List<OrdenVenta> ordenVentas = ordenVentaRepositorio.GetAllBy(x => x.EmpleadoCreacionId == user.UserId && x.FechaCreacion.Year == hoy.Year && x.FechaCreacion.Month == hoy.Month && x.FechaCreacion.Day == hoy.Day);
                    IngresoDetalle ingresoDetalle = model.Detalles.SingleOrDefault(x => x.TipoIngresoId == SettingsManager.tipoIngresoTiendaId);
                    decimal cantidadTotal = ingresoDetalle != null ? ingresoDetalle.Cantidad : 0;

                    return Content(JsonConvert.SerializeObject(new
                    {
                        ok = cantidadTotal == ordenVentas.Sum(x => x.Total),
                        cantidadIngreso = cantidadTotal,
                        cantidadVenta = ordenVentas.Sum(x => x.Total)
                    }), "application/json");
                }
                else
                {
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    return PartialView("_Error");
                }
            }
            catch (Exception ex)
            {
                ViewData["ErrorMessage"] = ex.Message;
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return PartialView("_Error");
            }
        }

        public ActionResult GetAllLikePagin(IngresoFiltro filter, int pageSize, int pageIndex)
        {
            try
            {
                int totalRows = 0;
                List<Ingreso> lista = ingresoRepositorio.GetAllLikePagin(filter, pageSize, pageIndex, ref totalRows);
                return Content(JsonConvert.SerializeObject(new
                {
                    lista = lista.Select(x => new
                    {
                        x.IngresoId,
                        x.EmpleadoNombreCompleto,
                        FechaRegistro = x.FechaRegistro.ToString("dd/MM/yyyy"),
                        Cantidad = x.Cantidad.ToString("###########0.00")
                    }),
                    totalRows
                }));
            }
            catch (Exception ex)
            {
                ViewData["ErrorMessage"] = ex.Message;
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return PartialView("_Error");
            }
        }

        #region Metodos

        public void ValidEntity(Ingreso item)
        {
            ModelState.Clear();
            if (item.Detalles != null && item.Detalles.Sum(x => x.Cantidad) < 0) ModelState.AddModelError("Cantidad", "La cantidad del ingreso no puede ser menor a 0.");
            if (item.EmpleadoId <= 0) ModelState.AddModelError("EmpleadoId", "Debe seleccionar un personal.");
        }

        #endregion
    }
}