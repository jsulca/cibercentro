﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Cibercentro.Helpers;
using Cibercentro.Entidades;
using Cibercentro.Repositorio;
using Newtonsoft.Json;
using Cibercentro.WebApp.Common;
using Cibercentro.Entidades.Filtro;

namespace Cibercentro.WebApp.Controllers
{
    public class PagoController : Controller
    {
        private readonly EmpleadoRepositorio empleadoRepositorio = new EmpleadoRepositorio();
        private readonly DescuentoRepositorio descuentoRepositorio = new DescuentoRepositorio();
        private readonly PagoRepositorio pagoRepositorio = new PagoRepositorio();

        [FiltroSeguridad]
        public ActionResult Index(int id)
        {
            List<Pago> pagos = pagoRepositorio.GetAllBy(x => x.EmpleadoId == id);
            Empleado empleado = empleadoRepositorio.GetOne(x => x.EmpleadoId == id);
            ViewBag.NombreCompleto = $"{empleado.Apellido}, {empleado.Nombre}";
            ViewBag.EmpleadoId = id;
            return View(pagos);
        }

        [FiltroSeguridad]
        public ActionResult Nuevo(int id)
        {
            try
            {
                Empleado empleado = empleadoRepositorio.GetOne(x => x.EmpleadoId == id);
                List<Descuento> descuentos = descuentoRepositorio.GetAllBy(x => x.EmpleadoId == id && x.Estado == EstadoDescuento.Pendiente);
                Pago model = new Pago()
                {
                    PagoId = 0,
                    EmpleadoId = id,
                    Desde = DateTime.Today.AddDays(-7),
                    Hasta = DateTime.Today,
                    SubTotal = empleado.Sueldo,
                    Total = empleado.Sueldo
                };
                ViewBag.NombreCompleto = $"{empleado.Apellido}, {empleado.Nombre}";
                ViewBag.Descuentos = descuentos;
                ViewBag.EmpleadoId = id;
                return View(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [FiltroSeguridad, HttpPost]
        public ActionResult Nuevo(Pago model, int[] descuentos)
        {
            try
            {
                ValidEntity(model);
                if (ModelState.IsValid)
                {
                    model.FechaCreacion = DateTime.Now;
                    model.EmpleadoCreacionId = ((UserInformation)Session["user"]).UserId;
                    model.Descuentos = new List<PagoDescuento>();
                    if(descuentos != null)
                    {
                        foreach (var d in descuentos)
                        {
                            model.Descuentos.Add(new PagoDescuento()
                            {
                                PagoId = 0,
                                DescuentoId = d
                            });
                        }
                    }
                    pagoRepositorio.Save(model);
                    return Json(new
                    {
                        mensaje = "El pago fue registrado.",
                        id = model.EmpleadoId
                    });
                }
                else
                {
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    return PartialView("_Error");
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return PartialView("_Error");
            }
        }

        public ActionResult Ver(int id)
        {
            try
            {
                ViewBag.Title = "Ver Detalle";
                Pago model = pagoRepositorio.GetOne(id);
                return PartialView("_Ver", model);

            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return PartialView("_Error");
            }
        }

        #region Metodos

        public void ValidEntity(Pago model)
        {
            ModelState.Clear();
            if (model.Desde > model.Hasta) ModelState.AddModelError("Desde", "Las fechas no son validas.");
        }

        #endregion
    }
}