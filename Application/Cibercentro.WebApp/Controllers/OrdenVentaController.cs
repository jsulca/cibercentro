﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Cibercentro.Helpers;
using Cibercentro.Entidades;
using Cibercentro.Repositorio;
using Newtonsoft.Json;
using Cibercentro.WebApp.Common;
using Cibercentro.Entidades.Filtro;

namespace Cibercentro.WebApp.Controllers
{
    public class OrdenVentaController : Controller
    {
        private readonly OrdenVentaRepositorio ordenVentaRepositorio = new OrdenVentaRepositorio();

        [FiltroSeguridad]
        public ActionResult Index()
        {
            return View();
        }

        [FiltroSeguridad]
        public ActionResult Editar(string id = null)
        {
            try
            {
                OrdenVenta model = null;
                if (string.IsNullOrEmpty(id))
                {
                    ViewBag.Title = "Nueva Orden de Venta";
                    model = new OrdenVenta() { FechaCreacion = DateTime.Now };
                }
                else
                {
                    ViewBag.Title = "Editar Orden de Venta";
                    model = ordenVentaRepositorio.GetOne(id);
                }
                return View(model);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        [FiltroSeguridad]
        [HttpPost]
        public ActionResult Editar(OrdenVenta model)
        {
            try
            {
                ValidEntity(model);
                if (ModelState.IsValid)
                {
                    bool agregar = string.IsNullOrEmpty(model.Codigo);
                    if (agregar)
                    {
                        model.Estado = EstadoOrdenVenta.Emitido;
                        model.EmpleadoCreacionId = ((UserInformation)Session["user"]).UserId;
                        model.FechaCreacion = DateTime.Now;
                    }

                    model.Total = model.Detalles.Sum(x => x.Cantidad * x.Precio);

                    ordenVentaRepositorio.Save(model);
                    return Json(new { mensaje = agregar ? "La venta fue registrada." : "La venta fue actualizada." });
                }
                else
                {
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    return PartialView("_Error");
                }
            }
            catch (Exception ex)
            {
                ViewData["ErrorMessage"] = ex.Message;
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return PartialView("_Error");
            }
        }

        public ActionResult Anular(string id)
        {
            try
            {
                OrdenVenta entity = ordenVentaRepositorio.GetOne(x => x.Codigo.Equals(id));
                if (Session["user"] != null)
                {

                    if (entity != null)
                    {
                        entity.EmpleadoAnulacionId = ((UserInformation)Session["user"]).UserId;
                        entity.FechaAnulacion = DateTime.Now;
                        entity.Estado = EstadoOrdenVenta.Anulado;
                        ordenVentaRepositorio.Anular(entity);
                        return Json(new { mensaje = "La venta fue anulada." });
                    }
                    else throw new Exception("La orden no existe.");
                }
                else throw new Exception("Porfavor ingrese al sistema.");
            }
            catch (Exception ex)
            {
                ViewData["ErrorMessage"] = ex.Message;
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return PartialView("_Error");
            }
        }

        public ActionResult Ver(string id)
        {
            try
            {
                OrdenVenta model = ordenVentaRepositorio.GetOne(id);
                ViewBag.Title = "Orden de venta: " + id;
                if (model == null) throw new Exception("El codigo no esta registrado.");
                return PartialView("_Ver", model);
            }
            catch (Exception ex)
            {
                ViewData["ErrorMessage"] = ex.Message;
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return PartialView("_Error");
            }
        }
        public ActionResult GetAllLikePagin(OrdenVentaFiltro filter, int pageSize, int pageIndex)
        {
            try
            {
                int totalRows = 0;
                List<OrdenVenta> lista = ordenVentaRepositorio.GetAllLikePagin(filter, pageSize, pageIndex, ref totalRows);
                return Content(JsonConvert.SerializeObject(new
                {
                    lista = lista.Select(x => new
                    {
                        x.Codigo,
                        Nombre = x.EmpleadoCreacionNombreCompleto,
                        FechaCreacion = x.FechaCreacion.ToString("dd/MM/yyyy HH:mm"),
                        Total = x.Total.ToString("############0.00"),
                        x.Estado
                    }),
                    totalRows
                }));
            }
            catch (Exception ex)
            {
                ViewData["ErrorMessage"] = ex.Message;
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return PartialView("_Error");
            }
        }

        #region Metodos

        public void ValidEntity(OrdenVenta item)
        {
            ModelState.Clear();
            if (item.Detalles == null || item.Detalles.Count == 0) ModelState.AddModelError("Detalles", "No puede registrar una venta vacia.");
            else if (item.Detalles.Any(x => x.Cantidad <= 0)) ModelState.AddModelError("Detalles", "Un item tiene como cantidad 0.");
        }

        #endregion
    }
}