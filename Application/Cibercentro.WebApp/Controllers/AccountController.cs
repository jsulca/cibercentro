﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cibercentro.Entidades;
using Cibercentro.Repositorio;
using Cibercentro.Helpers;
using Cibercentro.WebApp.Common;

namespace Cibercentro.WebApp.Controllers
{
    public class AccountController : Controller
    {
        private readonly EmpleadoRepositorio empleadoRepositorio = new EmpleadoRepositorio();
        private readonly RolRepositorio rolRepositorio = new RolRepositorio();
        public ActionResult Login(string ReturnUrl)
        {
            ViewBag.ReturnUrl = ReturnUrl;
            return View(new Empleado() { TipoDocumento = TipoDocumento.DNI });
        }

        [HttpPost]
        public ActionResult Login(Empleado model, string ReturnUrl)
        {
            try
            {
                ValidEntity(model);
                if (ModelState.IsValid)
                {
                    Empleado empleado = empleadoRepositorio.GetOne(x => x.TipoDocumento == model.TipoDocumento && x.NroDocumento.Equals(model.NroDocumento));
                    if (empleado != null)
                    {
                        if (empleado.Clave.Equals(CryptographyHelper.Encrypt(model.Clave)))
                        {
                            UserInformation user = new UserInformation() { UserId = empleado.EmpleadoId, Name = $"{empleado.Apellido}, {empleado.Nombre}" };
                            Rol rol = rolRepositorio.GetOneByLogin(empleado.RolId);
                            user.Menus = rol.Menus.Select(x => new MenuInformation() { Tipo = x.Menu.Tipo, MenuId = x.MenuId, Nombre = x.Menu.Nombre, PadreId = x.Menu.PadreId, Url = x.Menu.Url }).ToList();
                            user.Pages = rol.Paginas.Select(x => new PageInformation() { PageId = x.PaginaId, Url = $"/{x.Pagina.Controlador}/{x.Pagina.Accion}" }).ToList();
                            user.Controles = rol.Controles.Select(x => new ControlInformation() { PageId = x.Control.PaginaId, Nombre = x.Control.Nombre }).ToList();
                            Session["user"] = user;
                            return Redirect(ReturnUrl ?? "/");
                        }
                        else throw new Exception("La clave es incorrecta.");
                    }
                    else throw new Exception("No existe un usuario con los datos ingresados.");

                }
                else
                {
                    ViewBag.ReturnUrl = ReturnUrl;
                    return View(model);
                }
            }
            catch (Exception ex)
            {
                ViewBag.ReturnUrl = ReturnUrl;
                ModelState.AddModelError("", ex.Message);
                return View(model);
            }
        }

        public ActionResult Logout()
        {
            Session.Abandon();
            return RedirectToAction("Login");
        }
        #region Metodos
        public void ValidEntity(Empleado item)
        {
            ModelState.Clear();
            if (item.TipoDocumento <= 0) ModelState.AddModelError("TipoDocumento", "Seleccione un tipo de documento.");
            if (string.IsNullOrEmpty(item.NroDocumento) || string.IsNullOrWhiteSpace(item.NroDocumento)) ModelState.AddModelError("NroDocumento", "Ingrese un nro de documento.");
            if (string.IsNullOrEmpty(item.Clave) || string.IsNullOrWhiteSpace(item.Clave)) ModelState.AddModelError("Clave", "Ingrese una clave.");
        }
        #endregion
    }
}