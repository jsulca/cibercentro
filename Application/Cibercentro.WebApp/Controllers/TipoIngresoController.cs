﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Cibercentro.Helpers;
using Cibercentro.Entidades;
using Cibercentro.Repositorio;
using Newtonsoft.Json;
using Cibercentro.WebApp.Common;

namespace Cibercentro.WebApp.Controllers
{
    public class TipoIngresoController : Controller
    {
        private readonly TipoIngresoRepositorio tipoIngresoRepositorio = new TipoIngresoRepositorio();

        [FiltroSeguridad]
        public ActionResult Index()
        {
            return View();
        }

        [FiltroSeguridad]
        public ActionResult Editar(int id = 0)
        {
            try
            {
                TipoIngreso model = null;
                if (id > 0)
                {
                    ViewData["Title"] = "Editar Tipo de Ingreso";
                    model = tipoIngresoRepositorio.GetOne(id);
                }
                else
                {
                    ViewData["Title"] = "Nuevo Tipo de Ingreso";
                    model = new TipoIngreso() { Estado = EstadoTipoIngreso.Habilitado };
                }

                return View(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost]
        public ActionResult Editar(TipoIngreso model)
        {
            try
            {
                ValidEntity(model);
                if (ModelState.IsValid)
                {
                    bool agregar = model.TipoIngresoId == 0;
                    tipoIngresoRepositorio.Save(model);
                    return Json(new { mensaje = (agregar ? "El tipo de ingreso fue registrado." : "El tipo de ingreso fue editado."), id = model.TipoIngresoId });
                }
                else
                {
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    return PartialView("_Error");
                }
            }
            catch (Exception ex)
            {
                ViewData["ErrorMessage"] = ex.Message;
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return PartialView("_Error");
            }
        }

        public ActionResult GetAllLikePagin(TipoIngreso filter, int pageSize, int pageIndex)
        {
            try
            {
                int totalRows = 0;
                List<TipoIngreso> lista = tipoIngresoRepositorio.GetAllLikePagin(filter, pageSize, pageIndex, ref totalRows);
                return Content(JsonConvert.SerializeObject(new
                {
                    lista = lista.Select(x => new
                    {
                        x.TipoIngresoId,
                        x.Nombre,
                        Estado = x.Estado.ToString()
                    }),
                    totalRows
                }));
            }
            catch (Exception ex)
            {
                ViewData["ErrorMessage"] = ex.Message;
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return PartialView("_Error");
            }
        }

        #region Metodos

        private void ValidEntity(TipoIngreso item)
        {
            ModelState.Clear();
            if (string.IsNullOrEmpty(item.Nombre) || string.IsNullOrWhiteSpace(item.Nombre)) ModelState.AddModelError("Nombre", "Ingrese un nombre.");
        }

        #endregion
    }
}