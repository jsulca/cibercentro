﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cibercentro.Entidades
{
    public class IngresoDetalle
    {
        public int IngresoId { get; set; }
        public int TipoIngresoId { get; set; }
        public decimal Cantidad { get; set; }

        public Ingreso Ingreso { get; set; }
        public TipoIngreso TipoIngreso { get; set; }
    }
}
