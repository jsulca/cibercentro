﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cibercentro.Entidades
{
    public partial class Pago
    {
        public int PagoId { get; set; }
        public DateTime Desde { get; set; }
        public DateTime Hasta { get; set; }
        public int EmpleadoId { get; set; }
        public int EmpleadoCreacionId { get; set; }
        public DateTime FechaCreacion { get; set; }
        public decimal SubTotal { get; set; }
        public decimal TotalDescuento { get; set; }
        public decimal Total { get; set; }

        public List<PagoDescuento> Descuentos { get; set; }
    }
}
