﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cibercentro.Entidades
{
    public class PagoDescuento
    {
        public int PagoId { get; set; }
        public int DescuentoId { get; set; }

        public Pago Pago { get; set; }
        public Descuento Descuento { get; set; }
    }
}
