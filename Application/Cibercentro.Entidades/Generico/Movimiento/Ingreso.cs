﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cibercentro.Entidades
{
    public partial class Ingreso
    {
        public int IngresoId { get; set; }
        public int EmpleadoId { get; set; }
        public DateTime FechaRegistro { get; set; }
        public decimal Cantidad { get; set; }
        public string Observacion { get; set; }

        public int EmpleadoCreacionId { get; set; }
        public int? EmpleadoActualizacionId { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime? FechaActualizacion { get; set; }

        public List<IngresoDetalle> Detalles { get; set; }
    }
}
