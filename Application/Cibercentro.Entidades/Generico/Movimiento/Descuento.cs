﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cibercentro.Entidades
{
    public partial class Descuento
    {
        public int DescuentoId { get; set; }

        public int EmpleadoId { get; set; }
        public decimal Cantidad { get; set; }
        public string Motivo { get; set; }
        public DateTime Fecha { get; set; }
        public EstadoDescuento Estado { get; set; }

        public int EmpleadoCreacionId { get; set; }
        public int? EmpleadoActualizacionId { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime? FechaActualizacion { get; set; }

        public List<PagoDescuento> Pagos { get; set; }
    }
}
