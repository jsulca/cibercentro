﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cibercentro.Entidades
{
    public enum EstadoTipoIngreso
    {
        Habilitado = 1,
        Inhabilitado = 2
    }

    public enum EstadoUsuario
    {
        Activo = 1,
        Inactivo = 2
    }

    public enum TipoDocumento
    {
        DNI = 1,
        PASAPORTE = 2
    }

    public enum EstadoRol
    {
        Activo = 1,
        Inactivo = 2
    }

    public enum EstadoPagina
    {
        Activo = 1,
        Inactivo = 2
    }

    public enum TipoMenu
    {
        Normal = 1,
        Header = 2,
        Collapse = 3,
        Item = 4
    }

    public enum EstadoDescuento {
        Pendiente = 1,
        Aplicado = 2,
        Anulado = 3
    }

    public enum EstadoOrdenVenta
    {
        Emitido = 1,
        Anulado = 2
    }

    public enum TipoEgreso
    {
        Costos = 1,
        Gastos = 2
    }
    
}
