﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cibercentro.Entidades
{
    public class Configuracion
    {
        public int ConfiguracionId { get; set; }
        public int CorrelativoOrdenVenta { get; set; }
    }
}
