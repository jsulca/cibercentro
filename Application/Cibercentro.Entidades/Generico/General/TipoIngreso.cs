﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cibercentro.Entidades
{
    public partial class TipoIngreso
    {
        public int TipoIngresoId { get; set; }
        public string Nombre { get; set; }
        public EstadoTipoIngreso Estado { get; set; }

        public List<IngresoDetalle> Ingresos { get; set; }
    }
}
