﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cibercentro.Entidades
{
    public partial class OrdenVenta
    {
        public string Codigo { get; set; }
        public DateTime FechaCreacion { get; set; }
        public int EmpleadoCreacionId { get; set; }
        public decimal Total { get; set; }
        public EstadoOrdenVenta Estado { get; set; }
        public int? EmpleadoAnulacionId { get; set; }
        public DateTime? FechaAnulacion { get; set; }

        public List<OrdenVentaDetalle> Detalles { get; set; }
    }
}
