﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cibercentro.Entidades
{
    public partial class OrdenVentaDetalle
    {
        public string CodigoOrdenVenta { get; set; }
        public int ProductoId { get; set; }
        public decimal Precio { get; set; }
        public int Cantidad { get; set; }

        public OrdenVenta OrdenVenta { get; set; }
        public Producto Producto { get; set; }
    }
}
