﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cibercentro.Entidades
{
    public partial class RolMenu
    {
        public int RolId{ get; set; }
        public int MenuId { get; set; }

        public Rol Rol { get; set; }
        public Menu Menu { get; set; }
    }
}
