﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cibercentro.Entidades
{
    public partial class Control
    {
        public int ControlId { get; set; }
        public int PaginaId { get; set; }
        public string Nombre { get; set; }

        public Pagina Pagina { get; set; }
        public List<RolControl> RolControl { get; set; }
    }
}
