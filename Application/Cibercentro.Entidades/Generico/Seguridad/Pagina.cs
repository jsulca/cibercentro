﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cibercentro.Entidades
{
    public partial class Pagina
    {
        public int PaginaId { get; set; }
        public string Nombre { get; set; }
        public string Controlador { get; set; }
        public string Accion { get; set; }
        public EstadoPagina Estado { get; set; }

        public List<Control> Controles { get; set; }
        public List<RolPagina> RolPagina { get; set; }
    }
}
