﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cibercentro.Entidades
{

    public partial class Rol
    {
        public int RolId { get; set; }
        public string Nombre { get; set; }
        public EstadoRol Estado { get; set; }

        public List<Empleado> Empleados { get; set; }

        public List<RolMenu> Menus { get; set; }
        public List<RolPagina> Paginas { get; set; }
        public List<RolControl> Controles { get; set; }
    }
}
