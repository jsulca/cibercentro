﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cibercentro.Entidades
{
    public partial class RolPagina
    {
        public int RolId { get; set; }
        public int PaginaId { get; set; }

        public Rol Rol { get; set; }
        public Pagina Pagina { get; set; }
    }
}
