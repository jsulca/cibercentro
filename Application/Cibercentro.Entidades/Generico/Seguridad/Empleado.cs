﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cibercentro.Entidades
{

    public class Empleado
    {
        public int EmpleadoId { get; set; }
        public int RolId { get; set; }
        public string Clave { get; set; }
        public EstadoUsuario Estado { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public TipoDocumento TipoDocumento { get; set; }
        public string NroDocumento { get; set; }
        public string Correo { get; set; }
        public string Telefono { get; set; }
        public string Celular { get; set; }
        public decimal Sueldo { get; set; }

        public Rol Rol { get; set; }
    }
}
