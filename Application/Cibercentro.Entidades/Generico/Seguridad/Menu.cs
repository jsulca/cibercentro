﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cibercentro.Entidades
{
    public partial class Menu
    {
        public int MenuId { get; set; }
        public int PadreId { get; set; }
        public string Nombre { get; set; }
        public string Url { get; set; }
        public string Icono { get; set; }
        public TipoMenu Tipo { get; set; }

        public List<RolMenu> RolMenu { get; set; }
    }
}
