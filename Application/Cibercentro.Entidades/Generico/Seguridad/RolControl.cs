﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cibercentro.Entidades
{
    public partial class RolControl
    {
        public int RolId { get; set; }
        public int ControlId { get; set; }

        public Rol Rol { get; set; }
        public Control Control { get; set; }
    }
}
