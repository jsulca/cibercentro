﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cibercentro.Entidades.Filtro
{
    public class IngresoFiltro
    {
        public string EmpleadoNombre { get; set; }
        public DateTime? FechaDesde { get; set; }
        public DateTime? FechaHasta { get; set; }
        public decimal? CantidadDesde { get; set; }
        public decimal? CantidadHasta { get; set; }
    }
}
