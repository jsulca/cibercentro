﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cibercentro.Entidades.Filtro
{
    public class OrdenVentaFiltro
    {
        public string Codigo { get; set; }
        public string Empleado { get; set; }
        public DateTime Fecha { get; set; }
    }
}
