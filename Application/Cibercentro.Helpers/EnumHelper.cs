﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cibercentro.Helpers
{
    public class EnumHelper
    {
        public static List<object> ToList<T>()
        {
            List<object> lista = new List<object>();
            string name;
            foreach (int i in Enum.GetValues(typeof(T)))
            {
                name = Enum.GetName(typeof(T), i);
                lista.Add(new { Name = name.Replace('_', ' '), Value = i });
            }
            return lista;
        }
        public static List<object> ToList<T>(int[] ex)
        {
            List<object> lista = new List<object>();
            string name;
            foreach (int i in Enum.GetValues(typeof(T)))
            {
                if (ex.Contains(i))
                {
                    name = Enum.GetName(typeof(T), i);
                    lista.Add(new { Name = name.Replace('_', ' '), Value = i });
                }
            }
            return lista;
        }
    }
}
